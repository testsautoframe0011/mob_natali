﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobTestFramework
{
    public class Drivers
    {
        public static IWebDriver dr;


        /// <summary>
        /// Create Driver
        /// </summary>
        /// <param name="browser">ch - create Chrome Driver; ch-hd - create Chrome Driver headless; fr - create Firefox Driver</param>
        public Drivers(string browser)
        {
            ChromeOptions optionsAll = new ChromeOptions();
            ChromeMobileEmulationDeviceSettings EmulSet = new ChromeMobileEmulationDeviceSettings();
            EmulSet.EnableTouchEvents = true;
            optionsAll.AddArgument("--start-maximized");
            optionsAll.AddArgument("--disable-notifications");
            optionsAll.AddArgument("--disable-notifications");
            optionsAll.EnableMobileEmulation("Pixel 2 XL");
            //optionsAll.AddArgument("--auto-open-devtools-for-tabs");

            switch (browser)
            {
                case "ch":
                    dr = new ChromeDriver(optionsAll);
                    break;
                case "ch-hd":
                    optionsAll.AddArgument("--headless");
                    dr = new ChromeDriver(optionsAll);
                    break;
                case "fr":
                    dr = new FirefoxDriver();
                    break;
            }
        }          
    }
}
