﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobTestFramework.Pages;

namespace MobTestFramework.General
{
    public class GeneralXPath : PageBase
    {

        public IWebElement BuyButton { get => WaitElement(By.XPath("//div[@id='product_detail_buy']"), 10); }
        public IWebElement BuyButtonUnable { get => WaitElement(By.XPath("//div[@id='product_detail_buy']/a[@class='circle disabled']"), 10); }

        public IWebElement ChangeColour { get => WaitElement(By.XPath("//div[@class='infos']//div[@class='one-color']/label[@class='']"), 10); }


        //первый размер из списка размеров
        public IWebElement ItemSize { get => WaitElement(By.XPath("//div[@class='one-color ']//label"), 10); }
        public IWebElement ItemSizeSecondOfList { get => WaitElement(By.XPath("(//div[@class='one-color ']//label)[2]"), 10); }
        public IWebElement ItemSizeThirdOfList { get => WaitElement(By.XPath("(//div[@class='one-color ']//label)[3]"), 10); }
        public IWebElement TextofSizeItem { get => WaitElement(By.XPath("//span[@id='product_detail_size']"), 10); }


        //Попап образи
        public IWebElement CloseObrazPopupButton { get => WaitElement(By.XPath("//div[contains(@class,'vue-popup__container')]//span[@class='vue-popup__close']"), 10); }
        public IWebElement InfoAboutObraz { get => WaitElement(By.XPath("//div[contains(@class,'vue-popup__container')]//div[@class='vue-popup__content']//div[@class='quick-view__navInfo']"), 10); }//div[@class='quick-view__navInfo']
        public List<IWebElement> ListOfPriceItemsObrazPopup { get => MyWaitElements(By.XPath("//a[contains(@class,'quick-view-card')]//span[contains(@class,'images-goods__current')]")); }
        public IWebElement PreviousObrazButton { get => WaitElement(By.XPath("//div[@class='owl-prev']"), 10); }
        public IWebElement NextObrazButton { get => WaitElement(By.XPath("//div[@class='owl-next']"), 10); }
        public List<IWebElement> ObrazPopupList { get => MyWaitElements(By.XPath("//div[@class='vue-popup__container']")); }
        public IWebElement TotalSummOfAmountObrazPopup { get => WaitElement(By.XPath("//div[contains(@class,'quick-view__total-amount')]/span[contains(@class,'images-goods__current')]"), 10); }//changed
        public IWebElement TextOfFirstItemInObrazPopup { get => WaitElement(By.XPath("//a//span[contains(@class,'quick-view-card__text')]")); }
        public List<IWebElement> ViewItemInObrazPopup { get => MyWaitElements(By.XPath("//div[contains(@class,'quick-view__detailed')]//span[@class='quick-view-card__top']"), 10); }//(//a[contains(@class,'quick-view__item')])[1]

        public IWebElement SliderFroward { get => WaitElement(By.XPath("//div[@class='owl-dot active']//following-sibling::div"), 10); }
        public IWebElement SliderBack { get => WaitElement(By.XPath("//div[@class='owl-dot active']//preceding-sibling::div[1]"), 10); }
    }
}
