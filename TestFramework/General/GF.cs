﻿using MobTestFramework.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace MobTestFramework.General
{
    public delegate IWebElement Condition(IWebElement element);
    public class GF
    {
        protected static IWebDriver Driver { get => Drivers.dr; }

        public static IWebElement WaitElement(IWebDriver Driver, By selector, int timeSeconds, Condition cnd)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeSeconds));
            return wait.Until((x) =>
            {
                IWebElement element = Driver.FindElement(selector);
                return cnd(element);
            });
        }

        public static IWebElement ExistDisplayedEnabled(IWebElement element)
        {
            return (element != null && element.Displayed && element.Enabled) ? element : null;
        }

        public static IWebElement ExistDisplayed(IWebElement element)
        {
            return (element != null && element.Displayed) ? element : null;
        }

        public static IWebElement Exist(IWebElement element)
        {
            return (element != null) ? element : null;
        }

        public static Func<IWebDriver, IWebElement> ElementIsClickable(By locator)
        {
            return Driver =>
            {
                var element = Driver.FindElement(locator);
                return (element != null && element.Displayed && element.Enabled) ? element : null;
            };
        }

        public static IWebElement WaitElementIsExist(IWebDriver Driver, By selector, int timeSeconds)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeSeconds));
            return wait.Until((x) =>
            {
                IWebElement element = Driver.FindElement(selector);
                return element;
            });
        }

        public static void ClickOn(IWebElement element, string description = "") { element.Click(); }

        public static void JavaScriptClick(IWebElement element, string description = "")
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)Driver;
            executor.ExecuteScript("arguments[0].click();", element);
        }

        /*        public static void CheckAction(bool check,string description = ""="")
                {
                    if (!check) { throw new ArgumentException("Check was false"); }
                }*/

        public static void CheckAction(bool check, string description = "", int timeSeconds = 10)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeSeconds));
            wait.Until((x) =>
            {
                //PageBase.WaitPageFullLoaded();
                return check == true;
            });

        }

        public static void WaitForClickabilityOfElement(IWebElement element, long timeToWait=10)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeToWait));
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public static void WaitForVisibility(By locator, string descr = "", double timeToWait = 10)
        {
            IWait<IWebDriver> wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeToWait));
            wait.Until(ExpectedConditions.ElementIsVisible(locator));
        }

        public static void SubmitBy(IWebElement element)
        {
            element.Submit();
        }

        public static String GetTextOfElement(IWebElement element, bool toLower = false)
        {
            if (toLower != true)
            {
                return element.Text;
            }
            else
            {
                return element.Text.ToLower();
            }
        }

        public static void SendKeys(IWebElement element, String keyword, string description = "")
        {
            element.Clear();
            element.SendKeys(keyword);
        }
        public static void SendKeysClickEnter(IWebElement element, String keyword, string description)
        {
            element.Click();
            element.Clear();
            element.SendKeys(keyword);
            element.SendKeys(Keys.Enter);
        }
        public static void SendKeysEnter(IWebElement element, String keyword, string description = "")
        {
            element.Clear();
            element.SendKeys(keyword);
            element.SendKeys(Keys.Enter);
        }

        public static void NavigateByJS(IWebElement element)
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
        }

        public static void SwitchTo()
        {
            Driver.SwitchTo().DefaultContent();
        }

        public static void Refresh()
        {
            Driver.Navigate().Refresh();
        }

        public static void Back()
        {
            Driver.Navigate().Back();
        }

        public static void MoveToElement(IWebElement element, string description = "")
        {
            Actions actions = new Actions(Driver);
            actions.MoveToElement(element).Build().Perform();
        }

        public static void MoveToElementAndClick(IWebElement element, string description = "")
        {
            Actions actions = new Actions(Driver);
            actions.MoveToElement(element).Build().Perform();
            actions.Click().Perform();
        }


        /*        public static void SlashElement(IWebElement element)//IWebElement webElement2/* int horizontal = 0, int vertical = 0*//*,string description= "")
                {
                    *//*            TouchAction action = new TouchAction((OpenQA.Selenium.Appium.Interfaces.IPerformsTouchActions)Driver);
                                action.Press(element).Wait().MoveTo(horizontal, vertical).Perform();

                                            IWebElement targetElement = _driver.FindElement(By.Id("droppable"));
                                IWebElement sourceElement = _driver.FindElement(By.Id("draggable"));

                                //this code can be able in page object model
                                var drag = _action
                                    .ClickAndHold(sourceElement)
                                    .MoveToElement(targetElement)
                                    .Release(targetElement)
                                    .Build();

                                //this code will be in test
                                drag.Perform();



                                TouchActions touchAction = new TouchActions(Driver);
                                touchAction.ClickAndHold(element).Build().Perform();
                                Thread.Sleep(1000);
                                touchAction.Scroll(element, horizontal, vertical).Release().Perform();*//*

                    *//*            Actions actions = new Actions(Driver);
                                actions.ClickAndHold(element).Build().Perform();
                                actions.
                                actions.MoveToElement(webElement2).Build().Perform();
                                actions.Release().Build().Perform();*//*

                    Actions actions = new Actions(Driver);
                    *//*            IWebElement targetElement = webElement2;*//*
                    IWebElement sourceElement = element;

                    //this code can be able in page object model
                    actions.MoveToElement(element).ClickAndHold().Perform();
                    actions. //.MoveByOffset(100, 100).Release().Perform();
                    //.Build();

                    //this code will be in test
                    //actions.Perform();
                }*/

        public static bool CheckElementNotVisible(IWebElement element)
        {
            for (int i = 0; i < 10; i++)
            {
                if (!element.Displayed) { return true; }
                Thread.Sleep(1000);
            }
            return false;
        }

        public static bool CheckElementNotExist(List<IWebElement> elements)
        {
            try
            {
                if (elements == null)
                {
                    return true;
                }

                for (int i = 0; i < 10; i++)
                {

                    if (!elements.Any())
                    { return true; }
                    Thread.Sleep(500);
                }
                return false;
            }
            catch (ArgumentNullException)
            {
                return true;
            }
        }

        public static bool CheckDisplayed(By el, string description = "")
        {

            for (int i = 0; i < 20; i++)
            {
                IWebElement element = Driver.FindElement(el);
                if (element.Displayed) { return true; }
                Thread.Sleep(500);
            }
            return false;
        }

        public static bool CheckDisplayedPag(By el, string description = "")
        {
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    IWebElement element = Driver.FindElement(el);

                    if (element.Displayed) { return true; }
                    Thread.Sleep(500);
                }
                catch (NoSuchElementException)
                {
                    return false;
                }
            }
            return false;
        }

        public static bool CheckCorrectText(By el, string str)
        {
            for (int i = 0; i < 15; i++)
            {
                IWebElement element = Driver.FindElement(el);
                if (element.Text == str) { return true; }
                Thread.Sleep(500);
            }
            return false;
        }

        public static void NavigateToUrl(string Url, string description = "")
        {
            Driver.Navigate().GoToUrl(Config.BaseUrl + Url);
        }

        /// <summary>
        /// Сортировка { Бестселлер - [1], Новинки - [2], Возрастание - [3] Убывание - [4] }
        /// Почта { Отделение - [0], К Дому - [1] }
        /// </summary>
        /// <param name="dropDown"></param>
        /// <param name="numberToClick"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static void DropDownClick(By dropDown, int numberToSelect, string description = "", int timeSeconds = 10)
        {
            SelectElement oSelect = new SelectElement(Driver.FindElement(dropDown));
            oSelect.SelectByIndex(numberToSelect);
            //PageBase.WaitPageFullLoaded();
        }

        public static void CheckElementNotAttachedToDom(IWebElement element, string description = "", int timeSeconds = 7)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeSeconds));
            try
            {
                wait.Until(ExpectedConditions.StalenessOf(element));
            }
            catch (Exception)
            { }
        }

        public static void CheckElementNotAttachedToDom2(List<IWebElement> elements, string description = "", int timeSeconds = 7)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeSeconds));
            try
            {
                foreach (var n in elements)
                {
                    wait.Until(ExpectedConditions.StalenessOf(n));
                }
            }
            catch (Exception)
            { }
        }
    }
}
