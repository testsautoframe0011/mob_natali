﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.General
{
    public class URLBuilder
    {

        protected static string Base { get => Config.Base; }

        public static URLBuilder URLBuilderInstance = new URLBuilder(Base, Localization);

        protected static string Localization { get => Config.Localization; }

        public static string BaseUrl;

        Dictionary<string, string> CurrentUrl =
            new Dictionary<string, string>()
            {
                 {"RU", ""},
                 {"UA", "ua/"},
                 {"EN", "en/"}
            };

        public override string ToString()
        {
            return Base + CurrentUrl[Localization];
        }

        /// <summary>
        /// Создание УРЛ - Базовая урла + язык со словаря
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="Lang"></param>
        public URLBuilder(string URL, string Lang)
        {
            switch (Lang)
            {
                case "RU":
                    BaseUrl = URL;
                    break;
                case "UA":
                    BaseUrl = (Base + CurrentUrl[Lang]).Trim();
                    break;
                case "EN":
                    BaseUrl = (Base + CurrentUrl[Lang]).Trim();
                    break;
            }
        }


    }
}
