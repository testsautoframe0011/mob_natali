﻿using MobTestFramework.General;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;

namespace MobTestFramework.Pages
{
    public class PageProfile : PageBase
    {
        public PageProfile() { Url = "personal/"; }

        //Menu
        public IWebElement ProfileMenu { get => WaitElement(By.XPath("//div[contains(@id,'personal_menu')]")); }


        public IWebElement CartProfileButton { get => WaitElement(By.XPath("//div[@id='personal_menu']//a[contains(text(),'Корзина')]")); }
        public IWebElement LogOutButton { get => WaitElement(By.XPath("//div[@id='personal_menu']//a[contains(text(),'Выход')]")); }
        public IWebElement PersonalDataButton { get => WaitElement(By.XPath("//div[@id='personal_menu']//a[contains(text(),'Личные')]")); }
        public IWebElement WishListButton { get => WaitElement(By.XPath("//div[@id='personal_menu']//a[contains(text(),'желаний')]")); }


        //WishListProfile
        public IWebElement DeleteFromWishListButton { get => WaitElement(By.XPath("//div[@class='del-fav']")); }
        public List<IWebElement> ItemsInWishList { get => Driver.FindElements(By.XPath("//div[@class='my-fav']//div[contains(@class,'one-cat-thum')]")).ToList(); }
        public IWebElement FirstItemInWishList { get => WaitElement(By.XPath("//div[@class='my-fav']//div[@class='one-news'][1]/div[contains(@class,'one-cat-thum')]")); }
        public IWebElement TitleOfFirstItemWishList { get => WaitElement(By.XPath("//div[@class='my-fav']//div[@class='one-cat-tit']/a")); }
        public IWebElement EmptyWishListText { get => WaitElement(By.XPath("//div[@class='my-fav'][contains(text(),'Список желаний пуст')]")); }



        //CartProfile
        public IWebElement CartPrifileItem { get => WaitElement(By.XPath("//div[@class='basket-row']")); }

        public IWebElement CartPrifileTitleZero { get => WaitElement(By.XPath("//div[@class='basket-all-info']")); }
        public List<IWebElement> DeleteFromCartProfileButton1 { get => MyWaitElements(By.XPath("//div[@class='basket-row']//div[@class='del-from']")); }

        public IWebElement DeleteFromCartProfileButton { get => WaitElement(By.XPath("(//div[@class='basket-row']//div[@class='del-from']/a[@data-id])[last()]")); }
        public IWebElement MakeOrderButtonCartPageProfile { get => WaitElement(By.XPath("//input[@class='border-black']")); }
        public IWebElement TitleOfItemInCartPageProfile { get => WaitElement(By.XPath("(//div[@class='basket-item-tit'])[last()]/a[last()]")); }
        public IWebElement DeletedtemFromCartPage { get => WaitElement(By.XPath("//div[@class='basket-row']//div[@class='del-from']")); }
        public IWebElement EmptyCartMessage { get => WaitElement(By.XPath("//div[@class='basket-all-info']")); }

        public By ProfileMessageOfEmptyCart = By.XPath("//div[@class='basket-all-info']");


        //PersonalInformation
        public IWebElement LastNameInput { get => WaitElement(By.XPath("//input[@name='PROFILE-LAST_NAME']")); }
        public IWebElement NameInput { get => WaitElement(By.XPath("//input[@name='PROFILE-NAME']")); }
        public IWebElement SavePersonalInfo { get => WaitElement(By.XPath("//button[@id='personal_save']")); }
        public IWebElement SuccessMessage { get => WaitElement(By.XPath("//span[@id='msg']")); }
        public IWebElement ProfileEmail { get => WaitElement(By.XPath("//input[@name='PROFILE-EMAIL']")); }


        //Незарегистрированный пользователь
        public IWebElement UnregistredUserMessage { get => WaitElement(By.XPath("//div[@class='lk-page']//p")); }



        public void DeleteAllItemsFromWishListAuth(string description)
        {
            if (!Driver.Url.Contains(Config.BaseUrl + Url))
            {
                Driver.Navigate().GoToUrl(Config.BaseUrl + Url);
            }
            WishListButton.Click();
            Thread.Sleep(500);
            var count = ItemsInWishList.Count;

            for (int i = 0; i < count; i++)
            {
                DeleteFromWishListButton.Click();
                //Thread.Sleep(1000);
                Driver.Navigate().Refresh();
                WishListButton.Click();
            }
        }

/*        public void DeleteAllItemsFromCartProfile(string description)
        {
            if (!Driver.Url.Contains(Config.BaseUrl + Url))
            {
                Driver.Navigate().GoToUrl(Config.BaseUrl + Url);//EnterProfileByClickOnIcon();
            }
            CartProfileButton.Click();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement element = null;
            wait.Until((d) =>
            {
                try
                {
                    List<IWebElement> elemList = d.FindElements(By.XPath("//div[@class='basket-row']")).ToList();
                    if (elemList.Count == 0) { throw new NoSuchElementException(); }
                    foreach (var element in elemList)
                    {
                        GF.MoveToElement(MakeOrderButtonCartPageProfile);
                        DeletedtemFromCartPage.Click();
                        Driver.Navigate().Refresh();
                    }

                    return element = d.FindElement(By.XPath("//div[@class='lk-page']"));

                }
                catch (NoSuchElementException)
                {
                    IWebElement elementFall = d.FindElement(By.XPath("//div[@class='lk-page']"));
                    return elementFall;
                }
            });
        }*/

        public void DeleteAllItemsFromCartProfile(string description)
        {
            if (!Driver.Url.Contains(Config.BaseUrl + Url))
            {
                Driver.Navigate().GoToUrl(Config.BaseUrl + Url);//EnterProfileByClickOnIcon();
            }
            CartProfileButton.Click();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement element = null;
            wait.Until((d) =>
            {
                try
                {
                    List<IWebElement> elemList = d.FindElements(By.XPath("//div[@class='basket-row']")).ToList();
                    if (elemList.Count == 0) { throw new NoSuchElementException(); }

                    for(int n = 0;n < elemList.Count;n++)
                    {
                        GF.MoveToElement(MakeOrderButtonCartPageProfile);
                        GF.ClickOn(DeleteFromCartProfileButton);
                        Driver.Navigate().Refresh();
                    }

                    return element = d.FindElement(By.XPath("//div[@class='lk-page']"));

                }
                catch (NoSuchElementException)
                {
                    IWebElement elementFall = d.FindElement(By.XPath("//div[@class='lk-page']"));
                    return elementFall;
                }
            });
        }
    }
}
