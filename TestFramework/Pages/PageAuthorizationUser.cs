﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using MobTestFramework.General;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Collections.Generic;

namespace MobTestFramework.Pages
{
    public partial class User : PageBase
    {
        public static User UserFirst = new User("aniartqa@gmail.com", "qaaniartgmailcom");
        public static User UserFacebook = new User("facebookqaaniart@gmail.com", "facebookqa");

        public User() { Url = ""; }

        public IWebElement AuthErrorMessage { get => WaitElement(By.XPath("//span[contains(text(),'Неверный логин или пароль')]"), 10); }
        public IWebElement AuthorizationPopup { get => WaitElement(By.XPath("(//div[@class='modal-body'])[1]"), 10); }
        public IWebElement EmailInput { get => WaitElement(By.XPath("//input[@name='AUTH-LOGIN']"), 10); }
        public IWebElement PasswordInput { get => WaitElement(By.XPath("(//input[@name='AUTH-PASSWORD'])[1]"), 10); }
        public IWebElement SignInByGoogleButton { get => WaitElement(By.XPath("//form[@id='auth_login']//a[@class='google']"), 10); }
        public IWebElement SubmitButton { get => WaitElement(By.XPath("//input[@id='auth_submit']"), 10); }

        public IWebElement GooglePlusButton { get => WaitElement(By.XPath("//div[@class='social-auth']//a[@class='google']")); }
        public IWebElement FacebookButton { get => WaitElement(By.XPath("//div[@class='social-auth']//a[@class='facebook']")); }

        public IWebElement GoogleMailinput { get => WaitElement(By.XPath("//input[@type='email']")); }
        public IWebElement GoogleDalee { get => WaitElement(By.XPath("//div[@id='identifierNext']")); }

        public IWebElement FacebookEmailInput { get => WaitElement(By.XPath("//input[contains(@id,'email')]")); }
        public IWebElement FacebookPassInput { get => WaitElement(By.XPath("//input[contains(@id,'pass')]")); }
        public IWebElement FacebookEnterButton { get => WaitElement(By.XPath("//button[contains(@data-sigil,'m_login_button')]")); }


        public void AuthorizationUser(User user, string description)
        {
            EnterProfileByClickOnIcon();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("(//div[@class='modal-body'])[1]")));
            EmailInput.SendKeys(user.Email);
            PasswordInput.SendKeys(user.Password);
            SubmitButton.Click();
            Driver.Navigate().Refresh();
            WaitPageFullLoaded();
        }

        public void FacebookAuth(User user, string description = "")
        {
            String MainWindow = Driver.CurrentWindowHandle;

            IReadOnlyCollection<string> GooglePlusWindows = Driver.WindowHandles;

            foreach (var c in GooglePlusWindows)
            {
                if (c == MainWindow)
                {
                    continue;
                }
                else
                {
                    Driver.SwitchTo().Window(c);
                    GF.SendKeys(FacebookEmailInput, UserFacebook.Email);
                    GF.SendKeys(FacebookPassInput, UserFacebook.Password);
                    GF.ClickOn(FacebookEnterButton);
                    WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
                    wait.Until((d) =>
                    {
                        GooglePlusWindows = Driver.WindowHandles;
                        return GooglePlusWindows.Count == 1;
                    });

                    Driver.SwitchTo().Window(MainWindow);
                }
            }
        }
    }

    public partial class User
    {
        public readonly string Email;
        public readonly string Password;

        public User(string email, string password)
        {
            Email = email;
            Password = password;
        }
    }
}
