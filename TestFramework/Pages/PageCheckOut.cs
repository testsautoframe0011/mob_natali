﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MobTestFramework.General;
using OpenQA.Selenium.Support.UI;

namespace MobTestFramework.Pages
{
    public class PageCheckOut : PageBase
    {
        public PageCheckOut() { Url = "order/order/"; }

        public string UrlBasket = "order/basket";

        public IWebElement CheckoutToCartButton { get => WaitElement(By.XPath("//div[@class='order-steps']/div[1]")); }


        public IWebElement CheckoutFeild { get => WaitElement(By.XPath("//div[@class='order-left ']//div[@class='your-order']")); }

        public IWebElement AuthSubmitButton { get => WaitElement(By.XPath("//input[@id='order-auth_submit']")); }
        public IWebElement FinishCheckoutButton { get => WaitElement(By.XPath("//button[contains(text(),'Оформить покупку')]")); }
        public IWebElement FreeDeliveryMessageCheckout { get => WaitElement(By.XPath("//div[@class='order-left ']//div[@class='sum-price-num'][not(contains(text(),'грн'))]")); }

        public IWebElement PaidDeliveryMessageCheckoutOfomlenie { get => WaitElement(By.XPath("//div[@class='order-sum-price']/div[@class='sum-row']//div[@class='sum-price-name' and contains(text(),'Доставка')]/following-sibling::div")); }
        public IWebElement PaidDeliveryMessageCheckoutBasket { get => WaitElement(By.XPath("//div[@class='basket-checkout-section-inner']//div[@class='sum-price-name' and contains(text(),'Доставка')]/following-sibling::div")); }

        public IWebElement ItemBasketInOrderPage { get => WaitElement(By.XPath("//div[@class='order-basket-role']")); }
        public IWebElement LoginUserInput { get => WaitElement(By.XPath("//input[@name='USER_LOGIN']")); }
        public IWebElement EmailUserInput { get => WaitElement(By.XPath("//input[@placeholder='E-MAIL']")); }


        public IWebElement PaidDeliveryMessageCheckout { get => WaitElement(By.XPath("//div[@class='order-sum-price']//div[@class='sum-price-num'][contains(text(),'50 грн')]")); }
        public IWebElement PasswordUserInput { get => WaitElement(By.XPath("//input[@name='USER_PASSWORD']")); }
        public IWebElement Order { get => WaitElement(By.XPath("//div[@id='sale_order']")); }
        public List<IWebElement> DeleteItemFromCheckOutButton { get => MyWaitElements(By.XPath("//i[@class='close']")); }

        public IWebElement ShowProductsButton { get => WaitElement(By.XPath("//div[@class='order-basket-top-sum']//following-sibling::div")); }
        public IWebElement CloseProductsButton { get => WaitElement(By.XPath("//div[@class='order-left ']//div[@onclick and contains(@class,active)]")); }


        public List<IWebElement> ProductsInCheckout { get => MyWaitElements(By.XPath("//div[@class='order-basket-role']//div[contains(@class,'one-your-order basket-product')]")); }

        //Social Media Authz
        public IWebElement FacebookCheckoutAutr { get => WaitElement(By.XPath("//div[@class='order-social-auth']/a[contains(@onclick,'facebook')]//img")); }


        //postal
        public IWebElement FirstBuyButton { get => WaitElement(By.XPath("//div[@class='log-bt']//a[@class='first-buy']")); }
        public IWebElement InputCity { get => WaitElement(By.XPath("//input[@name='ORDER_PROP_4']")); }
        public IWebElement DropDownPostList { get => WaitElement(By.XPath("//select[@name='ORDER_PROP_8']")); }

        public By DropDownWayOfDelivery = By.XPath("//select[@class='order-delivery-select']");
        public By DropDownPost = By.XPath("//select[@name='ORDER_PROP_8']");
        public By DropDownQuantity = By.XPath("//div[@data-entity='basket-item-quantity-block']//select[@data-entity='basket-item-sku-select']");


        public IWebElement TextOfPost { get => WaitElement(By.XPath("(//select[@name='ORDER_PROP_8']/following-sibling::div/div)")); }

        public IWebElement ChoosePostOfficeButton { get => WaitElement(By.XPath("(//div[@class='jq-selectbox__select']//div[@class='jq-selectbox__select-text'])[4]")); }
        public IWebElement Podskazka { get => WaitElement(By.XPath("(//div[@class='one-order-form'])[4]")); }

        public IWebElement InputStreet { get => WaitElement(By.XPath("//input[@name='ORDER_PROP_5']")); }
        public IWebElement InputHomeAddress { get => WaitElement(By.XPath("//input[@name='ORDER_PROP_6']")); }
        public IWebElement InpuApartment { get => WaitElement(By.XPath("//input[@name='ORDER_PROP_7']")); }

        public IWebElement CartCheckoutButton { get => WaitElement(By.XPath("//div[@class='order-steps']/div[1]")); }
        public IWebElement OrderCheckoutButton { get => WaitElement(By.XPath("//div[@class='order-steps']/div[2]")); }

        public List<IWebElement> PaymentWays { get => MyWaitElements(By.XPath("(//div[@data-id])[position()>4]")); }
        public IWebElement CashPaymentActive { get => WaitElement(By.XPath("//div[@class='one-pay-radio checked'][@data-id='2']")); }
        public IWebElement CardPaymentActive { get => WaitElement(By.XPath("//div[@class='one-pay-radio checked'][@data-id='3']")); }

        public IWebElement GeneralPrice { get => WaitElement(By.XPath("//div[@class='sum-row sum-total']/div[@class='sum-price-num sum-price-total']")); }
        public IWebElement PriceOfFirstItem { get => WaitElement(By.XPath("(//div[@class='one-your-order-descr']//div/div[@class='one-your-order-item-param'][4])[1]")); }
        public IWebElement PriceOfSecondItem { get => WaitElement(By.XPath("(//div[@class='one-your-order-descr']//div/div[@class='one-your-order-item-param'][4])[2]")); }


        public void DeleteElementsCheckout(string descriptionint, int quantityToNotDelete = 0)
        {
            GF.Refresh();
            GF.ClickOn(ShowProductsButton, "Раскрываю список товаров");

            Thread.Sleep(500);
            var delButton = DeleteItemFromCheckOutButton.Count;

            var before_count = DeleteItemFromCheckOutButton.Count;

            for (int i = 0; i < delButton + 1; i++)
            {
                if (delButton == quantityToNotDelete)
                {
                    break;
                }

                GF.ClickOn(DeleteItemFromCheckOutButton.ElementAt(0), "");
                Thread.Sleep(3000);
                delButton--;
                try
                {
                    GF.ClickOn(ShowProductsButton, "Раскрываю список товаров");
                    GF.CheckAction(delButton != before_count);
                }
                catch (Exception)
                {                 
                }

            }
        }

        //наичнает добавлдять со 2го єлемента
        public List<string> DropDown(By dropDown, string descriptionint = "")
        {
            List<string> str = new List<string>();
            SelectElement oSelect = new SelectElement(Driver.FindElement(dropDown));
            IList<IWebElement> elementCount = oSelect.Options;

            for (int i = 1; i < elementCount.Count; i++)
            {
                str.Add(elementCount.ElementAt(i).Text);
            }
            return str;
        }

        public void FillInputFields(string Street, string Home, string Apartment, string descriptionint = "")
        {
            GF.SendKeys(InputStreet, Street);
            GF.SendKeys(InputHomeAddress, Home);
            GF.SendKeys(InpuApartment, Apartment);
        }

        public bool TextWasInput(string Street, string Home, string Apartment)
        {
            return !InputStreet.Text.Contains(Street) || !InputHomeAddress.Text.Contains(Home) || !InpuApartment.Text.Contains(Apartment);
        }

        public bool PostIsDisplayed(int chooseElementAt = 2, string descriptionint = "")
        {
            GF.ClickOn(ChoosePostOfficeButton);
            string str;
            SelectElement oSelect = new SelectElement(Driver.FindElement(DropDownPost));
            IList<IWebElement> elementCount = oSelect.Options;
            try
            {

                str = elementCount.ElementAt(chooseElementAt).Text;
                elementCount.ElementAt(chooseElementAt).Click();
                GF.ClickOn(ChoosePostOfficeButton);
            }
            catch (ArgumentOutOfRangeException)
            {
                str = elementCount.ElementAt(10).Text;
                elementCount.ElementAt(10).Click();
                GF.ClickOn(ChoosePostOfficeButton);
            }
            return TextOfPost.Text.ToLower().Trim() == str.ToLower().Trim();
        }

        public bool ChangePayment(bool ClickOnFirst = true, string descriptionint = "")
        {
            if (ClickOnFirst == true)
            {
                PaymentWays.ElementAt(0).Click();
                return CashPaymentActive.Displayed;
            }
            else
            {
                PaymentWays.ElementAt(1).Click();
                return CardPaymentActive.Displayed;
            }
        }
    }
}
