﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobTestFramework.General;
using MobTestFramework.Tests;
using System.Text.RegularExpressions;
using System.Globalization;
using TestFramework.General;

namespace MobTestFramework.Pages
{
    public partial class PageBase
    {
        protected static IWebDriver Driver { get => Drivers.dr; }
        protected string BaseUrl { get => Config.BaseUrl; }

        protected static int timeLoadPage = 10;

        public string Url;
        public PageBase() { }

        public void Navigate() { WaitPageFullLoaded(); Driver.Navigate().GoToUrl(Config.BaseUrl + Url); }

        protected static IWebElement WaitElement(By selector, int time = 10)
        {
            WaitPageFullLoaded();
            return GF.WaitElement(Driver, selector, time, new Condition(GF.ExistDisplayedEnabled));
        }

        protected static IWebElement WaitElement(By selector, int time, Condition cnd)
        {
            WaitPageFullLoaded();
            return GF.WaitElement(Driver, selector, time, cnd);
        }

        /// <summary>
        /// Wait until a page is fully loaded
        /// </summary>
        /// <param name="time"></param>
        public static void WaitPageFullLoaded()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeLoadPage));

            wait.Until((x) =>
            {
                return ((IJavaScriptExecutor)Driver).ExecuteScript("return document.readyState").Equals("complete");
            });
        }

        public static List<IWebElement> MyWaitElements(By locator, long timeToWait = 10)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeToWait));
            try
            {
                return wait.Until((x) =>
                {
                    List<IWebElement> list = Driver.FindElements(locator).ToList();
                    if (list.Count() == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return list;
                    }
                });
            }
            catch (Exception ex)
            {
                if (ex is NoSuchElementException || ex is TimeoutException)
                {
                    return null;
                }
                else { return null; }
            }
        }

        //Sorts (використовується для перевірки чи правильно відсортований товар за ціною)
        public bool SortPrice(IList<IWebElement> elements, bool sort)
        {
            bool assert = false;
            IWebElement firstElement = elements.ElementAt(0);
            int numberFirst = GetNumberPrice(firstElement);
            if (sort == true)
            {
                for (int i = 1; i < (elements.Count); i++)
                {
                    int numberSecond = GetNumberPrice(elements.ElementAt(i));
                    if (numberFirst <= numberSecond) numberFirst = numberSecond;
                    else return assert;
                }
            }
            else
            {
                for (int i = 1; i < (elements.Count); i++)
                {
                    int numberSecond = GetNumberPrice(elements.ElementAt(i));
                    if (numberFirst >= numberSecond) numberFirst = numberSecond;
                    else return assert;
                }
            }
            return assert = true;
        }

        public bool SortPrice2(IList<IWebElement> elements, bool vozrastanie)
        {
            bool assert = false;
            IWebElement firstElement = elements.ElementAt(0);
            double numberFirst = GetNumberPrice2(firstElement);
            if (vozrastanie == true)
            {
                for (int i = 1; i < (elements.Count); i++)
                {
                    double numberSecond = GetNumberPrice2(elements.ElementAt(i));
                    if (numberFirst <= numberSecond) numberFirst = numberSecond;
                    else return assert;
                }
            }
            else
            {
                for (int i = 1; i < (elements.Count); i++)
                {
                    double numberSecond = GetNumberPrice2(elements.ElementAt(i));
                    if (numberFirst >= numberSecond) numberFirst = numberSecond;
                    else return assert;
                }
            }
            return assert = true;
        }

        public int GetNumberPriceRegex(IWebElement elementNumber, bool move = false, string description = "")
        {
            // (\d +.\d +)
            if (move == true)
            {
                GF.MoveToElement(elementNumber);
            }
            String elem = elementNumber.Text.Replace(" ", "");

            var final = Regex.Match(elem, @"[+-]?\d*[\.\,]?\d+", RegexOptions.Singleline).Value;
            int d = int.Parse(final, CultureInfo.InvariantCulture);

            return d;
        }

        public int GetNumberPrice(IWebElement elementNumber)
        {
            String elem = elementNumber.Text;
            var split = elem.Split(" ");
            int number;
            if (split.Length < 3) number = Int32.Parse(split[0]);
            else number = Int32.Parse(split[0] + split[1]);
            return number;
        }
        public int GetNumberPriceInt(IWebElement elementNumber, bool move = false, string description = "")
        {
            // (\d +.\d +)
            if (move == true)
            {
                GF.MoveToElement(elementNumber);
            }
            String elem = elementNumber.Text.Replace(" ","");

            var final = Regex.Match(elem, @"[+-]?\d*[\.\,]?\d+", RegexOptions.Singleline).Value;

            return int.Parse(final);
        }

        public double GetNumberPrice2(IWebElement elementNumber, bool move = false, string description = "")
        {
            // (\d +.\d +)
            if (move == true)
            {
                GF.MoveToElement(elementNumber);
            }
            String elem = elementNumber.Text;

            var final = Regex.Match(elem, @"[+-]?\d*[\.\,]?\d+", RegexOptions.Singleline).Value;
            double d = double.Parse(final, CultureInfo.InvariantCulture);

            return d;
        }

        //Comparing Lists (порівняння двох листів)
        public bool ComparingTwoLists(List<string> el1, List<string> el2)
        {
            if (el1.Count != el2.Count)
            {
                return false;
            }
            for (int i = 0; i < el1.Count; i++)
            {
                if (el1.ElementAt(i) != (el2.ElementAt(i)))
                    return false;
            }
            return true;
        }

        public List<string> MakeNewList(List<IWebElement> l)
        {
            List<string> str = new List<string>();
            for (int i = 0; i < l.Count; i++) { str.Add(l.ElementAt(i).Text); }
            return str;
        }

        //проверка фильтра по цене
        public bool CheckPriceFilter(IList<IWebElement> elements, string min, string max)
        {
            int minPrice = int.Parse(min);
            int maxPrice = int.Parse(max);
            int summ = 0;
            for (int i = 0; i < (elements.Count); i++)
            {
                int numberFirst = GetNumberPrice(elements.ElementAt(i));
                if ((numberFirst >= minPrice) && (numberFirst <= maxPrice)) summ++;
                else summ = 0;
            }
            if (summ == elements.Count) return true;
            else return false;
        }

        public bool CheckPriceFilterMy(IList<IWebElement> elements, string min, string max)
        {
            int minPrice = int.Parse(min);
            int maxPrice = int.Parse(max);
            int summ = 0;
            for (int i = 0; i < (elements.Count); i++)
            {
                int numberFirst = GetNumberPriceInt(elements.ElementAt(i));
                if ((numberFirst >= minPrice) && (numberFirst <= maxPrice)) summ++;
                else summ = 0;
            }
            if (summ == elements.Count) return true;
            else return false;
        }

        //сума елементів в образі 
        public int SummOfPricesOfObrazItems(IList<IWebElement> elements)
        {
            int summ = 0;
            for (int i = 0; i < (elements.Count); i++)
            {
                int numberFirst = GetNumberPrice(elements.ElementAt(i));
                summ += numberFirst;
            }
            return summ;
        }

        //Comparing Lists (порівняння двох листів) SORRTED
        public bool ComparingTwoListsSorted(List<string> el1, List<string> el2)
        {
            if (el1.Count != el2.Count)
            {
                return false;
            }

            for (int i = 0; i < el1.Count; i++)
            {
                if (!el1.Contains(el2[i]))
                    return false;
                //if (el1.ElementAt(i) != (el2.ElementAt(i)))
                //    return false;
            }
            return true;
        }

        public bool ComparingTwoListsBeElements(List<string> el1, List<string> el2)
        {
            if (el1.Count == 0 || el2.Count == 0)
            {
                return true;
            }

            for (int i = 0; i < el1.Count; i++)
            {


                if (el1.ElementAt(i) != (el2.ElementAt(i)))
                {
                    return false;
                }
            }
            return true;
        }
    }

    public partial class PageBase
    {

        public readonly string PlatyaUrl = "/catalog/platya/";
        public readonly string NovinkiUrl = "/catalog/novye_postupleniya/";
        public readonly string BazoviyPlatyaUrl = "/catalog/bazovyy_ofis/bazovye_platya/";
        public readonly string PolubotinkiUrl = "/catalog/obuv/polubotinki/";
        public readonly string BotinkiSumkiUrl = "/catalog/obuv/";
        public readonly string SaleUrl = "/sale/";
        public readonly string BazJacketeUrl = "/catalog/bazovyy_ofis/bazovye_zhakety_i_zhilety/";
        public readonly string BluziTunikiUrl = "/catalog/bluzy_i_tuniki/";
        public readonly string TopyiUrl = "/catalog/topy/";


        public static IWebElement CloseBanner { get => WaitElement(By.XPath("//div[contains(@class,'hs-spinner-modal')]//div[@class='hs-close-button']")); }



        public IWebElement CartButton { get => WaitElement(By.XPath("//div[@class='small-basket__icon']")); }
        public IWebElement MainLogo { get => WaitElement(By.XPath("//img[contains(@src,'logo')]")); }
        public IWebElement HamburgerButton { get => WaitElement(By.XPath("//div[@class='container']//div[contains(@class,'show-hide-menu')]"), 30); }

        //Ways for entering profile
        public IWebElement IconOfProfileButton { get => WaitElement(By.XPath("//div[contains(@class,'header-menu-in')]//a[contains(@class,'bt-1')]"), 15); }

        public IWebElement ProfileListButton { get => WaitElement(By.XPath("(//div[contains(@class,'show-mobile-menu')]//div[contains(@class,'footer-tit')])[3]")); }
        public IWebElement ProfileClosedListButton { get => WaitElement(By.XPath("(//div[contains(@class,'show-mobile-menu')]//div[@class='footer-tit'])[3]")); }
        public IWebElement ProfileOpenedListButton { get => WaitElement(By.XPath("//div[contains(@class,'show-mobile-menu')]//div[@class='footer-tit opened']")); }


        public IWebElement EnterCartByUncoverListButton { get => WaitElement(By.XPath("(//div[contains(@class,'show-mobile-menu')]//div[contains(@class,'footer-tit')])[3]/following-sibling::ul//li[1]")); }
        public IWebElement EnterWishListByUncoverListButton { get => WaitElement(By.XPath("//div[contains(@class,'show-mobile-menu')]//div[@class='footer-tit opened']/following-sibling::ul//li[2]")); }

        public IWebElement UnregistredUserProfileButton { get => WaitElement(By.XPath("(//div[contains(@class,'show-mobile-menu')]//div[contains(@class,'footer-tit')])[3]/following-sibling::ul//a")); }


        public void EnterProfileByClickOnIcon(string description = "")
        {
            GF.ClickOn(HamburgerButton);
            GF.ClickOn(IconOfProfileButton);//.Click();
        }

        //Search (пошук по сайту)
        public IWebElement GoSearchButton { get => WaitElement(By.XPath("//input[@class='search-bt']")); }
        public IWebElement SearchButton { get => WaitElement(By.XPath("//a[@class='bt-2']")); }
        public IWebElement SearchInput { get => WaitElement(By.XPath("//input[@id='title-search-input']")); }


        //MainMenu
        public IWebElement BazovyyOfisButton { get => WaitElement(By.XPath("(//ul//a[@href='/catalog/bazovyy_ofis/'])[1]")); }
        public IWebElement BlogButton { get => WaitElement(By.XPath("//ul//a[@href='/blog/']")); }
        public IWebElement CatalogButton { get => WaitElement(By.XPath("//ul//a[@href='/catalog/']")); }
        public IWebElement NashyTrendyButton { get => WaitElement(By.XPath("//ul//a[@href='/nashy_trendy/trendy/']")); }
        public IWebElement NovyePostupleniyaButton { get => WaitElement(By.XPath("//ul//a[@href='/catalog/novye_postupleniya/']")); }
        public IWebElement ObuvSumkiButton { get => WaitElement(By.XPath("(//ul//a[@href='/catalog/obuv/'])[1]")); }
        public IWebElement OurShopsButton { get => WaitElement(By.XPath("(//ul//a[@href='/our-shops/'])[1]")); }
        public IWebElement SaleButton { get => WaitElement(By.XPath("//ul//a[@href='/sale/']")); }

        //Open Sub-Menu buttons
        public IWebElement OpenCatalogSubMenuButton { get => WaitElement(By.XPath("(//li[contains(@class,'has-sub')]/span)[1]")); }
        public IWebElement OpenBazovyyOfusSubMenuButton { get => WaitElement(By.XPath("(//li[contains(@class,'has-sub')]/span)[2]")); }
        public IWebElement OpenObuvISumkiSubMenuButton { get => WaitElement(By.XPath("(//li[contains(@class,'has-sub')]/span)[3]")); }
        public IWebElement OpenNashiTrendySubMenuButton { get => WaitElement(By.XPath("(//li[contains(@class,'has-sub')]/span)[4]")); }
        public IWebElement OpenSaleSubMenuButton { get => WaitElement(By.XPath("(//li[contains(@class,'has-sub')]/span)[5]")); }



        //Sub-menu 
        public IWebElement AksessuaryButton { get => WaitElement(By.XPath("//a[@href='/catalog/aksessuary/']")); }
        public IWebElement BazovyeBluzyTopyButton { get => WaitElement(By.XPath("//li/a[@href='/catalog/bazovye_bluzy_i_topy/']")); }
        public IWebElement BazovyeBryukiButton { get => WaitElement(By.XPath("//li/a[@href='/catalog/bazovyy_ofis/bazovye_bryuki/']")); }
        public IWebElement BazovyePlatyaButton { get => WaitElement(By.XPath("//a[text()='Базовые платья']")); }
        public IWebElement BazovyeYubkiButton { get => WaitElement(By.XPath("//li/a[@href='/catalog/bazovyy_ofis/bazovye_yubki/']")); }
        public IWebElement BluzyITunikiButton { get => WaitElement(By.XPath("//li/a[@href='/catalog/bluzy_i_tuniki/']")); }
        public IWebElement BotinkiButton { get => WaitElement(By.XPath("//a[@href and contains(text(),'Ботинки')]")); }
        public IWebElement GolfySvitshotyButton { get => WaitElement(By.XPath("//a[@href='/catalog/golfy_i_svitshoty/']")); }
        public IWebElement GolfyButton { get => WaitElement(By.XPath("//a[@href='/catalog/golfy_i_svitshoty/golfy/']")); }
        public IWebElement KostyumyButton { get => WaitElement(By.XPath("//li/a[@href='/catalog/kostyumy/']")); }
        public IWebElement PlatyaButton { get => WaitElement(By.XPath("//li/a[@href='/catalog/platya/']")); }
        public IWebElement PolubotinkiButton { get => WaitElement(By.XPath("//li/a[@href='/catalog/obuv/polubotinki/']")); }
        public IWebElement SumkiButton { get => WaitElement(By.XPath("//a[@href='/catalog/obuv/sumki/']")); }
        public IWebElement TopyButton { get => WaitElement(By.XPath("//div[@class='sub-menu-in']//a[@href='/catalog/topy/']")); }
        public IWebElement TrikotazhButton { get => WaitElement(By.XPath("//div[@class='sub-menu-in']//a[@href='/catalog/trikotazh/']")); }
        public IWebElement TufliButton { get => WaitElement(By.XPath("//li/a[@href='/catalog/obuv/tufli/']")); }
        public IWebElement VerkhnyayaOdezhdaButton { get => WaitElement(By.XPath("//li/a[@href='/catalog/verkhnyaya_odezhda/']")); }
        public IWebElement YubkiButton { get => WaitElement(By.XPath("//a[@href='/catalog/yubki/']")); }
        public IWebElement ZhaketyIZhiletyButton { get => WaitElement(By.XPath("//a[text()='Жакеты / жилеты']")); }

        //Sale sub-menu
        public IWebElement DaysAyear365SaleButton { get => WaitElement(By.XPath("//li/a[@href='/sale/365_days_a_year/']")); }
        public IWebElement KostyumySaleButton { get => WaitElement(By.XPath("//li/a[@href='/sale/kostyumy/']")); }
        public IWebElement ObuvSaleButton { get => WaitElement(By.XPath("//li/a[@href='/sale/obuv/']")); }
        public IWebElement OutletSaleButton { get => WaitElement(By.XPath("//li/a[@href='/sale/outlet/']")); }
        public IWebElement PlatyaSaleButton { get => WaitElement(By.XPath("//li/a[@href='/sale/platya/']")); }
        public IWebElement TrikotazhSaleButton { get => WaitElement(By.XPath("//li/a[@href='/sale/trikotazh/']")); }
        public IWebElement YubkiSaleButton { get => WaitElement(By.XPath("//li/a[@href='/sale/yubki/']")); }
        public IWebElement ZhaketyZhiletySaleButton { get => WaitElement(By.XPath("//li/a[@href='/sale/zhakety_i_zhilety/']")); }

        //Nashi trend
        public IWebElement BlackWhiteOfficeButton { get => WaitElement(By.XPath("//li/a[@href='/nashy_trendy/black-white-autumn/']")); }
        public IWebElement GidPoStilyuButton { get => WaitElement(By.XPath("//li/a[@href='/nashy_trendy/gid-po-stilyu/']")); }
        public IWebElement KapsulnyyGarderobButton { get => WaitElement(By.XPath("//li/a[@href='/nashy_trendy/kapsulnyy-garderob-dlya-ofisa/']")); }
        public IWebElement NashiTkaniButton { get => WaitElement(By.XPath("//li/a[@href='/nashy_trendy/nashi_osnovnye_tkani/']")); }
        public IWebElement StayWarmButton { get => WaitElement(By.XPath("//li/a[@href='/nashy_trendy/30-gotovykh-obrazov-dlya-moroznykh-dney/']")); }
        public IWebElement ThreeHundredDayPerYear { get => WaitElement(By.XPath("//li/a[@href='/nashy_trendy/365-collection/']")); }
        public IWebElement Vesna21Button { get => WaitElement(By.XPath("//li/a[@href and contains(text(),'Look Book spring 21')]")); }
        public IWebElement OfficeСollection { get => WaitElement(By.XPath("(//li/a[@href='/nashy_trendy/office_new_collection/'])[1]")); }


        //Banners
        public IWebElement FirstBigBannerButton { get => WaitElement(By.XPath("//div[contains(@class,'top-banner')]//*[text()='За новинками']")); }
        public IWebElement FirstItemBanner { get => WaitElement(By.XPath("//div[@class='owl-item active']")); }

        //CartPopup

        public IWebElement PriceFirstItemMiniCart { get => WaitElement(By.XPath("(//div[@class='product-card-mini__bottom']//span[@class='def-price'])[1]")); }
        public IWebElement PriceSecondItemMiniCart { get => WaitElement(By.XPath("(//div[@class='product-card-mini__bottom']//span[@class='def-price'])[2]")); }
        public IWebElement GeneralPriceMiniCart { get => WaitElement(By.XPath("//div[@class='small-basket-inner__prices']//span[@class='def-price']")); }

        public IWebElement CartCloseButton { get => WaitElement(By.XPath("//div[@class='small-basket-inner__header']//span[@class='small-basket-inner__close']")); }

        public IWebElement TitleOfItemInMiniCart { get => WaitElement(By.XPath("//a[@class='product-card-mini__name'][last()]")); }

        public IWebElement ItemInCartPage { get => WaitElement(By.XPath("(//a[@class='basket-item-image-link'])[last()]")); }

        public IWebElement DeleteItemFromPopupCartButton { get => WaitElement(By.XPath("//span[@class='product-card-mini__delete']")); }
        public IWebElement FirstElementInCartPopup { get => WaitElement(By.XPath("//div[@class='product-card-mini']//img")); }
        public IWebElement GoToCartButton { get => WaitElement(By.XPath("//a[@class='def-button']")); }
        public IWebElement EmptyMiniCart { get => WaitElement(By.XPath("//div[@class='small-basket-inner'][count(div)<3]")); }

        public IWebElement NewArrivalsWishlistButton { get => WaitElement(By.XPath("//div[@class='def-like']")); }

        public IWebElement OpenedCartPopUp { get => WaitElement(By.XPath("//div[@class='small-basket-inner']")); }
        public IWebElement CartAmount { get => WaitElement(By.XPath("//div[@id='basket_small_amount']/span")); }

        public By CartAmountSelector = By.XPath("//span[@class='small-basket__counter']");
        public By CartEmptySelector = By.XPath("//div[@class='small-basket__icon' and not(descendant::span)]");
        public IWebElement CartAmountEmpty { get => WaitElement(By.XPath("//div[@class='small-basket__icon' and not(descendant::span)]")); }

        public bool GetQuantityCartPopup(string quantityMustBe)
        {
            try
            {
                return GF.CheckCorrectText(CartAmountSelector, quantityMustBe);
            }
            catch (NoSuchElementException)
            {
                IWebElement EmptyCart = Driver.FindElement(CartEmptySelector);
                return !EmptyCart.Displayed;
            }
        }

    }
}
