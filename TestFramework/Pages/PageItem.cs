﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MobTestFramework.General;

namespace MobTestFramework.Pages
{
    public class PageItem : PageBase
    {
        public PageItem() { Url = ""; }

        public IWebElement AddToWishListButtonPegeItem { get => WaitElement(By.XPath("//a[@class='fav ']"), 10); }
        public IWebElement ItemIsAddedToWishListPegeItem { get => WaitElement(By.XPath("//a[contains(@class,'fav  in-favorite')]"), 10); }
        public IWebElement ArticleOfItemInPageItem { get => WaitElement(By.XPath("//div[@class='product-descr']//div[@class='art']"), 10); }
        public IWebElement ChangeTypeOfSizesButton { get => WaitElement(By.XPath("//ul[@class='size-set']/li[not(@class='selected')]"), 10); }
        public IWebElement ColourOfItem { get => WaitElement(By.XPath("//div[@class='infos']//div[@class='prod-color-name']/span[last()]"), 10); }
        public IWebElement Errore_Size_Message { get => WaitElement(By.XPath("//span[@id='product_detail_size']"), 10); }
        public IWebElement NameOfItemInPageItem { get => WaitElement(By.XPath("//div[@class='product-descr']//*[contains(@class,'h-1') or contains(@class,'h1')]"), 10); }
        public List<IWebElement> SizesPageItemList { get => MyWaitElements(By.XPath("//div[@class='one-color ']//span"),10); }
        public IWebElement SizeTableButton { get => WaitElement(By.XPath("//div[@class='prod-table']/a"), 10); }
        public IWebElement SizeTablePopup { get => WaitElement(By.XPath("//div[@id='size-table']"), 10); }

        public IWebElement BackButton { get => WaitElement(By.XPath("//div[@id='section-back']"), 10); }

        public IWebElement СomplementTheImage { get => WaitElement(By.XPath("//div[@class='images-goods-vue dbKey1']/*/*"), 10); }

        public By СomplementTheStyle = By.XPath("//div[@class='images-goods-vue dbKey1']/*/*");

        public IWebElement PriceOfItem { get => WaitElement(By.XPath("//div[@class='prod-price']"), 10); }

        public IWebElement ItemColor { get => WaitElement(By.XPath("//div[@class='product-descr']//div[@class='prod-color-name']/span[2]"), 10); }

        public IWebElement TitleOfItemInPageItem { get => Driver.FindElement(By.XPath("//div[@class='product-descr']//h1")); }


        //Availability in store (наявність в магазині)
        public IWebElement AvailabilityInStore { get => WaitElement(By.XPath("//div[@class='prod-more']/a"), 10); }
        public IWebElement ShopsText { get => WaitElement(By.XPath("//div[@class='prod-detail-city-block']"), 10); }
        public IList<IWebElement> ListOfCity { get => MyWaitElements(By.XPath("//div[@class='town-shop-sel-in']//a"),10); }


        //Viewed Slider
        public IWebElement AddSliderItemToWishList { get => WaitElement(By.XPath("//a[@class='one-cat-fav ']"), 10); }
        public IWebElement ViewedProductsSliderItem { get => WaitElement(By.XPath("//div[contains(@class,'product-detail-viewed floated')]//div[contains(@class,'owl-item active')]")); }
        public IWebElement ViewedProductsSliderItemAddToFavorite { get => WaitElement(By.XPath("//div[contains(@class,'product-detail-viewed floated')]//div[contains(@class,'owl-item active')]//*[@class='favorite' or @id='favorite']")); }
       

        //Sticky Bar
        public IWebElement AddItemFromStickyBarButton { get => WaitElement(By.XPath("//div[contains(@class,'sticky-bar shown')]//div[contains(@class,'sticky-add-bag')]"), 10); }
        public IWebElement NataliFooterLogo { get => WaitElement(By.XPath("//div[@class='foot-bot']//div[@class='container']//div[@class='copy']")); }
        public By NataliFooterLogoSelector = By.XPath("//div[@class='foot-bot']//div[@class='container']//div[@class='copy']");



        public string GetArticleOfItem(IWebElement elementNumber)
        {
            string elem = elementNumber.Text;
            var split = elem.Split(" ");
            string number;
            number = split[1] + " " + split[2];
            return number;
        }


        public bool CheckTheDifferenceInStoreAddresses()
        {
            int count = 0;

            string noProduct = "Данного размера нет в наличии в этом городе.";
            string number = "Телефон";

            for (int i = 0; i < ListOfCity.Count; i++)
            {
                Thread.Sleep(1000);
                GF.ClickOn(ListOfCity[i], "");
                Thread.Sleep(1000);
                if (ShopsText.Text.Contains(noProduct))
                {
                    count++;
                }
                else
                {
                    if (ShopsText.Text.Contains(number))
                    {
                        return true;
                    }
                    else return false;
                }
            }

            if (count == 4)
            {
                return false;
            }
            else return true;
        }
    }
}
