﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;

namespace MobTestFramework.Pages
{
    public class PageCart : PageBase
    {
        public PageCart() { Url = "order/basket/"; }

        public IWebElement DeleteItemButton { get => WaitElement(By.XPath("//span[@class='basket-item-actions-remove']")); }
        public IWebElement DeliveryText { get => WaitElement(By.XPath("(//div[@class='sum-row sum-row__block '])[last()]")); }
        public IWebElement ItemInCartPage { get => WaitElement(By.XPath("(//a[@class='basket-item-image-link'])[last()]")); }
        public IWebElement MakeOrderButtonCartPage { get => WaitElement(By.XPath("//button[contains(text(),'Перейти к оформлению')]")); }
        public IWebElement ReestablishItemButton { get => WaitElement(By.XPath("//a[@data-entity='basket-item-restore-button']")); }
        public IWebElement TitleOfItemInCartPage { get => WaitElement(By.XPath("//div[@class='basket-items-list']//a[@class='one-your-order-item-name']")); }
        public IWebElement TextOfFreeDelivery { get => WaitElement(By.XPath("//div[contains(@class,'sum-row sum-row__block')]")); }
        public IWebElement ItemsInCart { get => WaitElement(By.XPath("//tr[@class='basket-items-list-item-container']")); }

        public IWebElement ColorOfItem { get => WaitElement(By.XPath("//div[contains(@class,'info-basket')]//div[@class='one-your-order-item-param'][1]")); }
        public IWebElement SizeOfItem { get => WaitElement(By.XPath("//div[contains(@class,'info-basket')]//div[@class='one-your-order-item-param'][2]")); }
        public IWebElement QuantityOfItem { get => WaitElement(By.XPath("//div[contains(@class,'info-basket')]//div[@class='one-your-order-item-param'][3]")); }
        public IWebElement PriceOfItem { get => WaitElement(By.XPath("//div[contains(@class,'info-basket')]//div[@class='one-your-order-item-param'][4]")); }

        //Promo-code
        public IWebElement PromoCodeButton { get => WaitElement(By.XPath("//div[contains(@class,'basket-checkout-block')]//span[@class='show-promo']")); }
        public IWebElement PromoCodeConfirmButton { get => WaitElement(By.XPath("//span[@class='basket-coupon-block-coupon-btn']")); }
        public IWebElement PromoCodeInput { get => WaitElement(By.XPath("//input[@data-entity='basket-coupon-input']")); }
        public IWebElement PromoCodeItem { get => WaitElement(By.XPath("//div[@class='basket-coupon-alert text-danger']")); }

        //Price
        public IWebElement GeneralPrice { get => WaitElement(By.XPath("(//div[@data-entity='basket-total-price'])[last()]"), 10); }
        public IWebElement PriceOfFirstItem { get => WaitElement(By.XPath("(//div[@class='one-your-order-item-param'][4])"), 10); }
        public IWebElement PriceOfSecondItem { get => WaitElement(By.XPath("(//div[@class='one-your-order-item-param'][4])[2]"), 10); }

        public IWebElement DeleteFromBasketButton { get => WaitElement(By.XPath("//div[@class='basket-item-block-actions']"), 10); }

        public List<IWebElement> ItemsInBasketList { get => MyWaitElements(By.XPath("//div[@class='one-your-order basket-product step-order']"), 10); }

        //public IWebElement DeletedItemMessage { get => WaitElement(By.XPath("//div[@class='basket-item-block-actions']")); }

        public By DeletedItemMessage = By.XPath("//div[@class='basket-items-list-item-removed-container']");



        public void DeleteAllItemsFromBasket(string description)
        {
            if (!Driver.Url.Contains(Config.BaseUrl + Url))
            {
                Driver.Navigate().GoToUrl(Config.BaseUrl + Url);
            }

            var count = ItemsInBasketList.Count;

            for (int i = 0; i < count; i++)
            {
                DeleteFromBasketButton.Click();

                Thread.Sleep(500);
                //Driver.Navigate().Refresh();
                //WishListButton.Click();
            }
        }

        // Exception коли повідомлення, про безкоштовну доставку не висвітлено, бо ціна менше 1500
        public bool DeliveryTextNotVisible()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement element = null;
            wait.Until<IWebElement>((d) =>
            {
                try
                {
                    element = TextOfFreeDelivery;
                    return element;
                }
                catch (Exception)
                {
                    IWebElement elementFall = ItemsInCart;
                    element = null;
                    return elementFall;
                }
            });
            if (element != null) return false;
            else return true;
        }
    }
}








/*        public bool DeliveryTextNotVisiable()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement element = null;
            wait.Until((d) =>
            {
                try
                {
                    if (DeliveryText.Displayed && DeliveryText.Enabled)
                    {
                        IWebElement elem = DeliveryText;
                        element = elem;
                        return element;
                    }
                    else return element;
                }
                catch (Exception)
                {
                    //IWebElement elementFall = d.FindElement(By.XPath("//tr[@class='basket-items-list-item-container']"));
                    element = null;
                    return element;
                }
            });
            if (element != null) return true;
            else return false;
        }*/