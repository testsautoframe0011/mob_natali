﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobTestFramework.Pages.PagesNashyTrendy
{
    public class PageOfficeCollection : PageBase
    {
        public PageOfficeCollection() { Url = "/nashy_trendy/office-new-collection/"; }

        public IWebElement ViewFirstObrazVesna21Button { get => WaitElement(By.XPath("(//div[contains(@class,'top-banner')]//following-sibling::img)[position()>1]")); }
        public IWebElement ViewMiddleObrazVesna21Button { get => WaitElement(By.XPath("(//div[contains(@class,'top-banner')]//following-sibling::img)[position()>5]")); }
    }
}
