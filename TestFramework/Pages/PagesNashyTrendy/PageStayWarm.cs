﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobTestFramework.Pages.PagesNashyTrendy
{
    public class PageStayWarm : PageBase
    {
        public PageStayWarm() { Url = "nashy_trendy/30-gotovykh-obrazov-dlya-moroznykh-dney/"; }

        public IWebElement ViewFirstObrazStayWarmButton { get => WaitElement(By.XPath("(//div[contains(@*,'cont')]//div[contains(@*,'banners desktop')]//div[@class='top-banner full'])//div"), 10); }//(//div[@class='bot-banner'])[1]//a[@class='set-btn']  ///html/body/div[2]/main/div/div/div/div[1]/div/div[1]/a/div/img

        public IWebElement ViewMiddleObrazStayWarmButton { get => WaitElement(By.XPath("(//div[contains(@*,'cont')]//div[contains(@*,'banners desktop')]//div[@class='top-banner full'])[15]//div/div"), 10); } //(//div[@class='bot-banner'])[last()]//a[@class='set-bt']

        public IWebElement GeneralObrazPicture { get => WaitElement(By.XPath("//div[@class='owl-item active']//img"), 10); }
    }
}