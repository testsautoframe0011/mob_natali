﻿using MobTestFramework.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.Pages.PagesNashyTrendy
{
    public class PageVesna21 : PageBase
    {
        public PageVesna21() { Url = "nashy_trendy/look-book-spring-21/"; }

        public IWebElement ViewFirstObrazVesna21Button { get => WaitElement(By.XPath("((//div[contains(@*,'cont')]//div[contains(@*,'banners desktop')]//div[@class='top-banner full'])//div[@class='one-sub'])[2]")); }
        public IWebElement ViewMiddleObrazVesna21Button { get => WaitElement(By.XPath("((//div[contains(@*,'cont')]//div[contains(@*,'banners desktop')]//div[@class='top-banner full'])//div[@class='one-sub'])[4]")); }       
    }
}
