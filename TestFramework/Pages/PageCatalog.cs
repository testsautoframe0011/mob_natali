﻿using MobTestFramework.General;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MobTestFramework.Pages
{
    public partial class PageCatalog : PageBase
    {
        public PageCatalog() { Url = ""; }


        public List<IWebElement> ProductItemList { get => MyWaitElements(By.XPath("//article[contains(@class,'product-card')]/div[@class='product-card__top']")); }
        public List<IWebElement> ItemWithFewColors { get => MyWaitElements(By.XPath("//article[contains(@class,'product-card')]//div[@class='offer-color__colors product-card__colors'][count(a)>1]//preceding-sibling::a[@class='product-card__name']")); }

        
        public List<IWebElement> ProductItemsTitlesList { get => MyWaitElements(By.XPath("//a[@class='product-card__name']")); }
        public List<IWebElement> ProductItemListPrice { get => MyWaitElements(By.XPath("//article[contains(@class,'product-card')]//div[@class='product-card__price-block']/span[last()]")); }


        public IWebElement ProductItem { get => WaitElement(By.XPath("(//div[contains(@class,'one-cat-thumb')])[1]")); }
        public IWebElement ProductItemSecond { get => WaitElement(By.XPath("(//div[contains(@class,'one-cat-thumb')])[2]")); }

        public IWebElement FirstItemWithColours { get => WaitElement(By.XPath("//div[contains(@class,'one-cat-color')]/parent::div")); }
        public IWebElement LastProductItem { get => WaitElement(By.XPath("(//div[contains(@class,'one-cat-item')])[last()]")); }
        public IWebElement EliteProductItem { get => WaitElement(By.XPath("//div[@class='one-cat-item cat-50 gtm-parent']")); }
        public List<IWebElement> ProductEliteItemsList { get => MyWaitElements(By.XPath("//div[@class='one-cat-item cat-50 gtm-parent']")); }
        public IWebElement ProductItemTitle { get => WaitElement(By.XPath("(//div[@class='one-cat-tit '])[1]//a[@class='gtm-link']")); }
        public IWebElement SearchErrorMessage { get => WaitElement(By.XPath("//h1[@class='catalog-top__search-empty']")); }


        //Adding to Wishlist
        public IWebElement AddToWishListButtonOfFirstItem { get => WaitElement(By.XPath("//div[@class='def-like']")); }
        public IWebElement AddToWishListButtonOfSecondItem { get => WaitElement(By.XPath("(//div[@class='def-like'])[2]")); }
        public IWebElement AddToWishListButtonOfThirdItem { get => WaitElement(By.XPath("(//div[@class='def-like'])[3]")); }
        public IWebElement AddToWishListButtonFromNewArrivals { get => WaitElement(By.XPath("//div[@id='novlety']//div[@class='owl-item active']//a[contains(@class,'one-cat-fav')]")); }
        public IWebElement DeleteFromWishListFirstItemButton { get => WaitElement(By.XPath("//div[contains(@class,'def-like_active')]")); }


        //Wishlist
        public IWebElement SelectItemFromWishList { get => WaitElement(By.XPath("//div[@class='my-fav']//div[contains(@class,'one-cat-thumb')]")); }
        public IWebElement ItemIsAddedToWishList { get => WaitElement(By.XPath("//div[@class='def-like def-like_active']")); }


        //Colours and Sizes
        public IWebElement ChangeColourButtonCatalog { get => WaitElement(By.XPath("//a[@class='offer-color__color']")); }
        public IWebElement ItemWithOtherColour { get => WaitElement(By.XPath("(//div[@class='one-cat-thumb'])[1]/div[@class='product_img ']/a[2][@style='display: inline;']")); }

        public IWebElement ProductItemSizes { get => WaitElement(By.XPath("(//div[@class='one-cat-sizes-aval '])[1]")); }
        public IWebElement ProductItemTitleWithColours { get => WaitElement(By.XPath("//div[contains(@class, 'one-cat-color')]/div[contains(@class, 'product_list_size one-color')][2]/parent::div/preceding-sibling::div[contains(@class, 'one-cat-tit')]/a")); }//изменил
        public IWebElement ProductItemWithColours { get => WaitElement(By.XPath("//div[@class='one-cat-color']/div[@class='product_list_size one-color'][2]/parent::div/parent::div")); }
        public List<IWebElement> SizesHoverEffectList { get => Driver.FindElements(By.XPath("(//div[contains(@class,'one-cat-item')])[1]//span[@class='one-avaliable-size']")).ToList(); }


        #region
        //Filters
        //public IWebElement UncoverFilterButton { get => WaitElement(By.XPath("//ul[@class='mob-filter-btn filter-open mob-filter ']")); }
        //public IWebElement ApplyFilterButton { get => WaitElement(By.XPath("//li[@id='apply-filter-mobile']")); }


        /*        public IWebElement ResetFilter { get => WaitElement(By.XPath("//ul[@//li[@id='reset-mobile-filter']")); }
                public IWebElement OpenSizesFilterButton { get => WaitElement(By.XPath("//li[@data-filter_group_code='sizes']/span")); }
                public IWebElement OpenColorFilterButton { get => WaitElement(By.XPath("//li[@data-filter_group_code='color']/span")); }
                public IWebElement OpenPriceFilterButton { get => WaitElement(By.XPath("//li[@data-filter_group_code='min_price']")); }*/


        /*        public IWebElement FilterHasBeenAppliedTop { get => WaitElement(By.XPath("//div[@class='multi-tit  selected_values']")); }
                public IWebElement FilterLeftHasBeenActive { get => WaitElement(By.XPath("//div[@id='filters_active']/ul")); }
                public IWebElement MoreFiltersButton { get => WaitElement(By.XPath("//li[@class='more-properties ']")); }
                public IWebElement ResetAllFiltersButton { get => WaitElement(By.XPath("//input[@id='custom_filter_reset']")); }*/




        /*        //Filter colour 
                public IWebElement BlackColourInFilterTopMenu { get => WaitElement(By.XPath("//li[@data-filter_group_code='color']//span[@data-original-title='Черный']")); }
                //public IWebElement ColoursFilterLeftMenu { get => WaitElement(By.XPath("//li[@data-filter_group_code='color']")); }
                //public IWebElement ColoursFilterTopMenu { get => WaitElement(By.XPath("//div[@class='multi-tit '][contains(text(),'Цвет')]/parent::div")); }
                public IWebElement RedColourInFilterLeftMenu { get => WaitElement(By.XPath("//li[@data-filter_group_code='color']//span[@data-original-title='Красный']")); }

                //Filter size
                public IWebElement ChangeTypeOfSizesButton { get => WaitElement(By.XPath("//div[@class='size-set-filter']/span[not(@class='selected')]")); }
                public IWebElement FirstSizeInFilterMenu { get => WaitElement(By.XPath("//ul[contains(@class,'filter-group')]//li[@class=' one-filt-element']")); }
                //public IWebElement FirstSizeInFilterTopMenu { get => WaitElement(By.XPath("//div[@class='filt-mobile ']//li[@class=' one-filt-element']")); }
                public IWebElement SizesFilterLeftMenu { get => WaitElement(By.XPath("//li[@data-filter_group_code='sizes']")); }
                public IWebElement SizesFilterTopMenu { get => WaitElement(By.XPath("//div[@class='multi-tit '][contains(text(),'Размеры')]/parent::div")); }

                //Filter price
                public IWebElement PriceRangeFilter { get => WaitElement(By.XPath("(//li/label[@class='range-set'])[3]")); }
                public IWebElement PriceMinFilter { get => WaitElement(By.XPath("//input[@id='text-min-value']")); }
                public IWebElement PriceMaxFilter { get => WaitElement(By.XPath("//input[@id='text-max-value']")); }*/
        #endregion

        //Pagination (переключення сторінок)

        public IWebElement SecondPageIsActive { get => WaitElement(By.XPath("//div[@id='catalog_pagination_num']//li[3][@class='active']")); }
        public IWebElement LookMorepaginationButton { get => WaitElement(By.XPath("//div[contains(@class,'add-more pagination')]")); }
        public By LookMorepaginationButtonSelector = By.XPath("//div[contains(@class,'add-more pagination')]");
        public IWebElement PaginationActivePage { get => WaitElement(By.XPath("//ul[@class='pagination__nav']/li[contains(@class,'active')]")); }
        public IWebElement PrevPagePaginationButton { get => WaitElement(By.XPath("//li[@class='pagination__button']//*[contains(@*,'left')]")); }
        public IWebElement NextPagePaginationButton { get => WaitElement(By.XPath("//li[@class='pagination__button']//*[contains(@*,'right')]")); }
        public List<IWebElement> PaginationElements { get => MyWaitElements(By.XPath("//ul[@class='pagination__nav']/li[not(@class='pagination__button') and not(contains(.,'...'))]")); }
        public IWebElement PaginationInput { get => WaitElement(By.XPath("//div[@id='catalog_pagination_num']//li[position()>1 and position()<last()]/span[@class='ellipse clickable']")); }


        public void NextPaginationElement(List<IWebElement> list, List<IWebElement> toChange, int elementat, string description = "")
        {
            try
            {
                var elBefore = toChange[0];

                GF.ClickOn(list[elementat]);
                GF.CheckElementNotAttachedToDom(elBefore);

            }
            catch (ArgumentOutOfRangeException)
            {
                var elBefore = toChange[0];
                GF.ClickOn(list[list.Count - 1]);
                GF.CheckElementNotAttachedToDom(elBefore);
            }
        }

        public void PrevPaginationElement(List<IWebElement> list, List<IWebElement> toChange, int amountPageReturn, string description = "")
        {
            try
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].GetAttribute("class") == "active")
                    {
                        var elBefore = toChange[0];

                        GF.ClickOn(list[i - amountPageReturn]);
                        GF.CheckElementNotAttachedToDom(elBefore);
                        break;
                    }
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException("Start from another element");
            }
        }

        public void NextElementButton(List<IWebElement> toChange, string description = "")
        {
            var elBefore = toChange[0];
            GF.MoveToElement(ProductItemList.Last());
            GF.MoveToElementAndClick(NextPagePaginationButton);
            GF.CheckElementNotAttachedToDom(elBefore);
        }

        public void PrevElementButton(List<IWebElement> toChange, string description = "")
        {
            var elBefore = toChange[0];

            GF.MoveToElementAndClick(PrevPagePaginationButton);
            GF.CheckElementNotAttachedToDom(elBefore);
        }

        //Filter Sort (сортування товара за ціною)

        public IWebElement CatalogSortSelect { get => WaitElement(By.XPath("//div[@class='jq-selectbox__trigger-arrow']")); }



        public void ClickOnVisibleProduct(List<IWebElement> list, string description = "")
        {
            foreach (var n in ProductItemList)
            {
                if (n.Displayed || n.Enabled)
                {
                    GF.ClickOn(n);
                    break;
                }
            }
        }

            public void FindProductWithAbilityToСomplementTheImage( string description="")//List<IWebElement> eliteElements,
        {
            PageItem p = new PageItem();

            for(int i=0;i< ProductItemList.Count;i++)
            {
                ProductItemList.ElementAt(i).Click();
                Thread.Sleep(1000);
                try
                {                   
                    if (!GF.CheckDisplayed(p.СomplementTheStyle))
                    {
                        GF.ClickOn(p.BackButton);
                        continue;
                    }
                    else break;
                }
                catch (Exception ex)
                {
                    if (ex is NoSuchElementException || ex is TimeoutException )
                    {
                        GF.ClickOn(p.BackButton);
                        continue;
                    }
                }
            }
        }

        public bool SearchResultContainsCorrectRequest(List<IWebElement> webElements, string textOfItem, string description = "")
        {
            if (webElements == null)
            {
                return false;
            }
            foreach (var item in webElements)
            {
                if (!item.Text.ToLower().Contains(textOfItem)) { return false; }
            }
            return true;
        }
    }

    public partial class PageCatalog : PageBase
    {
        /*//FILETRS//*/

        public List<IWebElement> ResultsBySortProducts { get => MyWaitElements(By.XPath("//div[@class='product-card__price-block']/span[last()]")); }

        public By FiltredPrice = By.XPath("//div[@class='product-card__price-block']/span[last()]");
        public IWebElement CatalogSortSelectRight { get => WaitElement(By.XPath("//div[@class='select-vue-sort']")); }
        public List<IWebElement> SortSelectRightChoice { get => MyWaitElements(By.XPath("//div[@class='select-vue-sort__list']/span")); }
        
        public IWebElement DeselectColor { get => WaitElement(By.XPath("//div[@class='filter-color__items']//span[@class='def-color__checkbox def-color__checkbox_checked']")); }

        public IWebElement OpenFilterButton { get => WaitElement(By.XPath("//div[@class='button-filter']")); }
        public IWebElement CloseFilterButton { get => WaitElement(By.XPath("//span[@class='filter-header__close']")); }

        

        public IWebElement OpenSortFilterButton { get => WaitElement(By.XPath("//div[@class='select-vue-sort']")); }

        //
        public IWebElement UbivanieSelectRight { get => WaitElement(By.XPath("//div[@class='select-vue-sort__list']/span[@class='select-vue-sort__link' and contains(text(),'Цена по убыванию')]")); }
        
        public IWebElement VozrastanieSelectRight { get => WaitElement(By.XPath("//div[@class='select-vue-sort__list']/span[@class='select-vue-sort__link' and contains(text(),'Цена по возрастанию')]")); }
        public IWebElement NoninkiSelectRight { get => WaitElement(By.XPath("//div[@class='select-vue-sort__list']/span[@class='select-vue-sort__link' and contains(text(),'Новинки')]")); }


        public List<IWebElement> DeselectFilterrButton { get => MyWaitElements(By.XPath("//div[@class='cancel-filter__items']/span")); }



        //Filters
        public IWebElement FilterHasBeenAppliedTop { get => WaitElement(By.XPath("//div[@class='catalog-top__filters']//div[@class='vue-select-color__header']/span[contains(text(),'')]")); }
        public IWebElement MoreFiltersButton { get => WaitElement(By.XPath("//div[@class='filter-big-block__header']")); }
        public IWebElement ResetAllFiltersButton { get => WaitElement(By.XPath("//a[@class='mobile-button__reset def-button-border']")); }

        //Main Filter colour 
        public IWebElement BlackColourInFilterLeftMenu { get => WaitElement(By.XPath("//div[@class='filter-color']//span[contains(text(),'Черный')]//preceding-sibling::span")); }
        public IWebElement ColoursFilterLeftMenu { get => WaitElement(By.XPath("//div[@class='vue-select-color__header']")); }
        public IWebElement ColoursFilterTopMenu { get => WaitElement(By.XPath("//div[@class='vue-select-color']")); }
        public IWebElement RedColourInFilterLeftMenu { get => WaitElement(By.XPath("//div[@class='filter-color']//span[contains(text(),'Красный')]//preceding-sibling::span")); }

        //Filter size
        public IWebElement ChangeTypeOfSizesButton { get => WaitElement(By.XPath("//div[@class='size-tabs__control']/span[not(contains(@class,'active'))]")); }
        public IWebElement FirstSizeInFilterLeftMenu { get => WaitElement(By.XPath("//div[@class='filter-size__items']/label[@class='filter-size__checkbox'][1]")); }
        public List<IWebElement> SizeInFilterList { get => MyWaitElements(By.XPath("//div[@class='filter-size__items']/label[@class='filter-size__checkbox']")); }

        public IWebElement FirstSizeInFilterTopMenu { get => WaitElement(By.XPath("//div[@class='vue-select-size__items']//label[1]")); }
        public IWebElement SizesFilterLeftMenu { get => WaitElement(By.XPath("//div[@id='custom_filter']//li[@data-filter_group_code='sizes']")); }
        public IWebElement SizesFilterTopMenu { get => WaitElement(By.XPath("//div[@class='vue-select-size__header']")); }
        public IWebElement FilterText { get => WaitElement(By.XPath("//span[@class='menu-name']")); }

        //Filter price
        public IWebElement OpenPriceFilter { get => WaitElement(By.XPath("//div[@class='filter-block__header']/*[contains(text(),'Цена')]")); }

        
        public IWebElement PriceMinFilter { get => WaitElement(By.XPath("//div[@class='catalog-range__block-input']//input[@data-name='MIN']")); }
        public IWebElement PriceMaxFilter { get => WaitElement(By.XPath("//div[@class='catalog-range__block-input']//input[@data-name='MAX']")); }

    }

}
