﻿using MobTestFramework.General;
using MobTestFramework.Pages;
using MobTestFramework.Tests;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.Pages;

namespace TestFramework.Tests.Catalog
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Каталог"), Property("TestSuite:", "CT")]
    class TestPagination<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Переключение на следующую страницу числом")]
        public void CT1_NextPageNumberClick()
        {
            GF.NavigateToUrl(PPattern.PlatyaUrl);
            var elBeforeText = PCatalog.ProductItemsTitlesList[0].Text;
            GF.MoveToElement(PCatalog.ProductItemList.Last());
            PCatalog.NextPaginationElement(PCatalog.PaginationElements, PCatalog.ProductItemList, 1);
            GF.CheckAction(PCatalog.ProductItemsTitlesList[0].Displayed);
            Assert.AreNotEqual(elBeforeText, PCatalog.ProductItemsTitlesList[0].Text);
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Переключение на следующую страницу числом, открытая страница активна(обозначена)")]
        public void CT2_NextPageNumberClick()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            GF.NavigateToUrl(PPattern.SaleUrl);
            GF.MoveToElement(PCatalog.ProductItemList.Last());
            PCatalog.NextPaginationElement(PCatalog.PaginationElements, PCatalog.ProductItemList, 1);
            Assert.IsTrue(PCatalog.PaginationElements[1].GetAttribute("class").Contains("active"));
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Вторая страница отображается после клика на Посмотреть еще")]
        public void CT3_SecondListDisplayedMoreItem()
        {
            GF.NavigateToUrl(PPattern.NovinkiUrl);
            var elBefore = PCatalog.ProductItemsTitlesList.Count;
            GF.MoveToElement(PCatalog.ProductItemList.Last());
            GF.ClickOn(PCatalog.LookMorepaginationButton, "Нажимаю на Показать еще");
            Thread.Sleep(2000);
            var elAfter = PCatalog.ProductItemsTitlesList.Count;
            Assert.AreNotEqual(elBefore, elAfter, "Следующие товары отображены");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Посмотреть еще не отображается на последней странице")]
        public void CT4_ShowMorenotDisplaedlastPage()
        {
            GF.NavigateToUrl(PPattern.SaleUrl);
            GF.MoveToElement(PCatalog.ProductItemList.Last());
            PCatalog.NextPaginationElement(PCatalog.PaginationElements, PCatalog.ProductItemList, 20);
            Assert.IsTrue(!GF.CheckDisplayedPag(PCatalog.LookMorepaginationButtonSelector), "Кнопка Посмотреть еще не отображается");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Посмотреть еще на второй странице")]
        public void CT5_NumberInputActive()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            GF.NavigateToUrl(PPattern.NovinkiUrl);
            GF.MoveToElement(PCatalog.ProductItemList.Last());
            PCatalog.NextPaginationElement(PCatalog.PaginationElements, PCatalog.ProductItemList, 1);
            var elBefore = PCatalog.ProductItemsTitlesList.Count;
            GF.ClickOn(PCatalog.LookMorepaginationButton, "Нажимаю на Показать еще");
            GF.CheckAction(PCatalog.PaginationElements[1].GetAttribute("class").Contains("active"));
            Thread.Sleep(2000);
            var elAfter = PCatalog.ProductItemsTitlesList.Count;
            Assert.AreNotEqual(elBefore, elAfter, "Следующие товары отображены");
        }
    }
}
