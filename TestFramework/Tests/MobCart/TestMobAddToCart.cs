﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using MobTestFramework.General;
using MobTestFramework.Pages;

namespace MobTestFramework.Tests.Cart
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "С")]
    class TestMobAddToCart<TBrowser> : TestBase<TBrowser>
    {
        private readonly string SearchText = "Юбка";

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в ПопАп корзины через страницу товара")]
        public void С1_AddItemToCartFromItemPage()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Разворачиваю список Каталога");
            GF.ClickOn(PPattern.PlatyaButton, "Выбираю Платья");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к первому товару");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            Assert.IsTrue(PPattern.FirstElementInCartPopup.Displayed, "Товар добавлен в попАп корзины");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в корзину профиля через страницу товара перейдя в нее через слайдер (просмотренные товары)")]
        public void С2_AddItemToCartPageProfileFromPageItemBySlider()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PPattern.EnterProfileByClickOnIcon("Переход в профиль");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.NovyePostupleniyaButton, "Выбираю Новинки");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к первому товару");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(PItem.ViewedProductsSliderItem, "Нажимаю на первый товар в слайдере Просмотренные товары");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            PPattern.EnterProfileByClickOnIcon("Переход в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.CartProfileButton, "Нажимаю на Корзину");
            Assert.IsTrue(PProf.CartPrifileItem.Displayed, "Товар добавлен в корзину");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в попап корзины через образ")]
        public void C3_AddItemToCartFromObraz()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenNashiTrendySubMenuButton, "Разворачиваю список Наши Тренды");
            GF.ClickOn(PPattern.OfficeСollection, "Выбираю Оставайся в тепле");
            Thread.Sleep(3000);
            GF.ClickOn(PV21.ViewFirstObrazVesna21Button, "Открываю первый образ"); 
            GF.ClickOn(GXP.ViewItemInObrazPopup[0], "Выбираю товар с образа");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            Assert.IsTrue(PPattern.FirstElementInCartPopup.Displayed, "Товар добавлен в попАп корзины");
        }


        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в попап корзины через StickyBar")]
        public void C4_AddItemToCartFromStickyBar()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Разворачиваю список Каталога");
            GF.ClickOn(PPattern.BluzyITunikiButton, "Выбираю Блузы и туники");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.MoveToElement(PItem.NataliFooterLogo, "направляюсь к кнопке Оставить отзыв");
            GF.WaitForVisibility(PItem.NataliFooterLogoSelector, "Жду появления отзывов");
            GF.ClickOn(PItem.AddItemFromStickyBarButton, "Нажимаю добавить в корзину через Липкое меню");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            Assert.IsTrue(PPattern.FirstElementInCartPopup.Displayed, "Товар добавлен в корзину");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в корзину профиля через страницу поисковой выдачи")]
        public void C5_AddItemToCartPageProfileFromSearchPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PPattern.EnterProfileByClickOnIcon("Переход в профиль");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.ClickOn(PPattern.SearchButton, "Нажимаю на поиск");
            GF.SendKeys(PPattern.SearchInput, SearchText, "Ввожу текст Юбки");
            GF.ClickOn(PPattern.GoSearchButton, "Выполняю поиск");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            PPattern.EnterProfileByClickOnIcon("Переход в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.CartProfileButton, "Нажимаю на Корзину");
            Assert.IsTrue(PProf.CartPrifileItem.Displayed, "Товар добавлен в корзину");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в корзину c листа желаний")]
        public void C6_AddItemToCartPageFromWishList()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            PProf.DeleteAllItemsFromWishListAuth("Удаление всех товаров с листа желаний");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.BazovyyOfisButton, "Выбираю Базовий офис");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к первому товару");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "Добавляю товар в лист желаний");
            PPattern.EnterProfileByClickOnIcon("Переход в профиль");
            GF.ClickOn(PProf.WishListButton, "Нажимаю на лист желаний");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PCatalog.SelectItemFromWishList, "Выбираю товар с листа желаний");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Перейти в корзину");
            GF.NavigateToUrl(PCart.Url);
            Assert.IsTrue(PCart.TitleOfItemInCartPage.Displayed, "Товар добавлен в корзину");
        }
        
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в корзину через Дополнить образ")]
        public void C7_AddItemToCartPageFromСomplementTheImage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю Каталог");
            GF.ClickOn(PPattern.TopyButton, "Выбираю Топы"); ;
            PCatalog.FindProductWithAbilityToСomplementTheImage();
            GF.ClickOn(PItem.СomplementTheImage,"Открываю дополнить образ");;
            GF.ClickOn(GXP.TextOfFirstItemInObrazPopup,"Открываю первый товар из образа");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Перейти в корзину");
            GF.NavigateToUrl(PCart.Url);
            Assert.IsTrue(PCart.TitleOfItemInCartPage.Displayed, "Товар добавлен в корзину");
        }
    }
}
