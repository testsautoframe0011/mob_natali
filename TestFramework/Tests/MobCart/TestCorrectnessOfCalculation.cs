﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using MobTestFramework.General;
using MobTestFramework.Pages;

namespace MobTestFramework.Tests.MobCart
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "C")]
    class TestCorrectnessOfCalculation<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка корректности расчёта доставки в корзине, доставка бесплатная")]
        public void C13_CalculationOfFreeDelivery()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.BazovyyOfisButton, "Выбираю Базовий офис");

            //filter
            var old = PCatalog.ProductItemList[0];
            GF.ClickOn(PCatalog.OpenSortFilterButton, "Раскрываю фильтр сортировки");
            GF.ClickOn(PCatalog.UbivanieSelectRight, "Выбираю сортировку Цена по Убыванию");
            GF.CheckElementNotAttachedToDom(old);

            GF.CheckAction(PCatalog.ProductItemList[0].Displayed, "Жду отображение товаров");
            PCatalog.ClickOnVisibleProduct(PCatalog.ProductItemList,"Нажимаю на товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            Thread.Sleep(2000);
            GF.CheckAction(PPattern.CartCloseButton.Displayed);
            GF.ClickOn(PPattern.CartCloseButton);
            GF.ClickOn(GXP.ItemSizeSecondOfList, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton);
            Assert.IsTrue(!PCart.DeliveryTextNotVisible(), "Текст о бесплатной доставке отображен");
        }


        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка корректности расчета доставки в корзине, сообщение о бесплатной доставке не видно, доставка платная")]
        public void C14_CalculationOfPaidDelivery()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю Каталог");
            GF.ClickOn(PPattern.YubkiButton, "Выбираю Юбки");

            //filter
            var old = PCatalog.ProductItemList[0];
            GF.ClickOn(PCatalog.OpenSortFilterButton, "Раскрываю фильтр сортировки");
            GF.ClickOn(PCatalog.VozrastanieSelectRight, "Выбираю сортировку Цена по Возрастанию");
            GF.CheckElementNotAttachedToDom(old);

            PCatalog.ClickOnVisibleProduct(PCatalog.ProductItemList, "Нажимаю на товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton);
            Assert.IsTrue(PCart.DeliveryTextNotVisible(), "Текст о не бесплатной доставке отображен");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка корректности расчёта суммы в корзины")]
        public void C15_CheckSummOfTwoItemsInCartPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.NavigateToUrl(PV21.Url, "Направляюсь к Офисной коллекции");
            GF.CheckAction(PV21.ViewFirstObrazVesna21Button.Displayed, "Жду загрузки трендов");
            GF.WaitForClickabilityOfElement(PV21.ViewFirstObrazVesna21Button);
            GF.ClickOn(PV21.ViewFirstObrazVesna21Button, "Открываю первый образ");
            GF.ClickOn(GXP.ViewItemInObrazPopup[0], "Выбираю товар с образа");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю Каталог");
            GF.ClickOn(PPattern.TrikotazhButton, "Выбираю Трикотаж");
            PCatalog.ClickOnVisibleProduct(PCatalog.ProductItemList, "Нажимаю на товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton);
            var final = PCart.GetNumberPriceRegex(PCart.PriceOfFirstItem) + PCart.GetNumberPriceRegex(PCart.PriceOfSecondItem); //Добавляю суммы товаров
            Assert.AreEqual(final, PCart.GetNumberPriceRegex(PCart.GeneralPrice), "Расчёт суммы корректен");
        }
    }
}
