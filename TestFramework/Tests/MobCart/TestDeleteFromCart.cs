﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using MobTestFramework.General;
using MobTestFramework.Pages;

namespace MobTestFramework.Tests.MobCart
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "С")]
    class TestDeleteFromCart<TBrowser> : TestBase<TBrowser>
    {
        private readonly string EmptyCartMessage = "В корзине пусто";

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление товара из попапа корзины")]
        public void C8_DeleteItemFromCart()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю Каталог");
            GF.ClickOn(PPattern.YubkiButton, "Выбираю Юбки");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к первому товару");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.DeleteItemFromPopupCartButton, "Удаляю товар с попАпа Корзины");
            Thread.Sleep(1000);
            GF.ClickOn(PPattern.CartButton, "Открываю малую корзину");
            Assert.IsTrue(PPattern.EmptyMiniCart.Displayed, "Товар удален с попАпа Корзины");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление товара из страницы корзины")]
        public void C9_DeleteItemFromCartPage()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю Каталог");
            GF.ClickOn(PPattern.YubkiButton, "Выбираю Юбки");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к первому товару");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Перейти в корзину");
            PCart.DeleteAllItemsFromBasket("Удаление всех товаров с корзины");
            Assert.IsTrue(GF.CheckDisplayed(PCart.DeletedItemMessage), "Товар удален c Корзини");
        }


        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление товара из корзины в профиле")]
        public void C10_DeleteItemFromCartPageProfile()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю Каталог");
            GF.ClickOn(PPattern.BluzyITunikiButton, "Выбираю Блузы и Туники");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к первому товару");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            PPattern.EnterProfileByClickOnIcon("Перехожу в Профиль");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.CheckAction(PProf.CartPrifileTitleZero.Displayed, "Жду загрузки корзины");
            Assert.IsTrue(GF.CheckCorrectText(PProf.ProfileMessageOfEmptyCart, EmptyCartMessage), "Товар удален из корзины в профиле");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление всех товаров на странице корзина")]
        public void C11_DeleteAllItemFromCartPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenObuvISumkiSubMenuButton, "Раскрываю Обувь и ботинки");
            GF.ClickOn(PPattern.BotinkiButton, "Выбираю Ботинки");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            PPattern.EnterProfileByClickOnIcon("Перехожу в Профиль");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            Assert.IsTrue(GF.CheckDisplayed(PProf.ProfileMessageOfEmptyCart), "Товар удален c Корзини");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Восстановление удаленного товара")]
        public void C12_RecovereDeletedItemInCartPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenObuvISumkiSubMenuButton, "Раскрываю Обувь и ботинки");
            GF.ClickOn(PPattern.TufliButton, "Выбираю Туфли");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(PItem.ViewedProductsSliderItem, "Нажимаю на товар в слайдере Просмотренные товары");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Перейти в корзину");
            PCart.DeleteAllItemsFromBasket("Удаление всех товаров с корзины");
            GF.CheckDisplayed(PCart.DeletedItemMessage,"Товар удален c Корзини");
            GF.ClickOn(PCart.ReestablishItemButton, "Восстанавливаю товар");
            Assert.IsTrue(PCart.TitleOfItemInCartPage.Displayed, "Товар восстановлен");
        }
    }
}
