﻿using MobTestFramework.General;
using MobTestFramework.Pages;
using MobTestFramework.Tests;
using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading;
using TestFramework.Pages;
using TestFramework.Tests.Cart;

namespace TestFramework.Tests.Cart.CartPopup
{

    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "С")]
    class TestCartPopupItemAmount<TBrowser> : TestBase<TBrowser>
    {
/*        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление двух одинаковых товаров в попап корзины 2 товара, изменение числа корзины")]
        public void С1_AddCartPopupTwoIdenticalCartAmountChange()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.NovyePostupleniyaButton, "Нажимаю на Новые поступления");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            GF.ClickOn(PPattern.CartCloseButton,"Закріваю малую Корзину");
            GF.ClickOn(GXP.ItemSizeSecondOfList, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            Assert.IsTrue(GF.CheckCorrectText(PPattern.CartAmountSelector, "2"), "Иконка корзины отображает наличие 2х товаров");
        }*/

/*        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в попап корзины 1 товар, изменение числа корзины")]
        public void С2_AddCartPopupTwoIdenticalCartAmountChange()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.SaleButton, "Нажимаю на Sale");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            Assert.IsTrue(GF.CheckCorrectText(PPattern.CartAmountSelector, "1"), "Иконка корзины отображает наличие добавленного товара");
        }*/

        /*        [Test]
                [Author("Dmytro Lytovchenko"), Description("Увеличение количества товаров на Чекауте, число в попапе увеличилось")]
                public void С3_IncreaseNumberCheckoutCartAmountChange()
                {
                    GF.ClickOn(PPattern.NovyePostupleniyaButton, "Нажимаю на Новые поступления");
                    GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
                    GF.ClickOn(GXP.ItemSize, "Выбираю размер");
                    GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
                    GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
                    GF.ClickOn(PPattern.GoToCartButton, "В попапе нажимаю Перейти в корзину");
                    GF.ClickOn(PCart.ChangeAmountOfItem, "Открываю список количества товаров");
                    //var beforeQuantity = PPattern.CartAmount.Text.Trim();
                    GF.ClickOn(PCheck.SelectSize(PCheck.DropDownQuantity, 1, "Изменяю количество до 2х товаров"));
                    //GF.CheckAction(!GF.CheckCorrectText(PPattern.CartAmountSelector, beforeQuantity),"Жду",);            
                    Assert.IsTrue(GF.CheckCorrectText(PPattern.CartAmountSelector, "2"), "Товар успішно доданий в попап корзини");
                }*/

        /*                [Test]
                        [Author("Dmytro Lytovchenko"), Description("Уменьшение количества товаров на Чекауте, число в попапе уменьшилось")]
                        public void С4_ReduceNumberCheckoutCartAmountChange()
                        {
                            GF.NavigateToUrl(PCatalog.PlatyaUrl);
                            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
                            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
                            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
                            GF.CheckAction(PPattern.FirstElementInCartPopup.Displayed);
                            GF.ClickOn(PPattern.GoToCartButton, "В попапе нажимаю Перейти в корзину");
                            GF.ClickOn(PCart.ChangeAmountOfItem, "Открываю список количества товаров");
                            //var beforeQuantity = PPattern.CartAmount.Text.Trim();
                            GF.ClickOn(PCheck.SelectSize(PCheck.DropDownQuantity, 1, "Увеличиваю количество до 2х товаров"));
                            GF.ClickOn(PCheck.SelectSize(PCheck.DropDownQuantity, 0, "Уменьшаю количество до 1х товаров"));
                            //GF.CheckAction(!GF.CheckCorrectText(PPattern.CartAmountSelector, beforeQuantity),"Жду",);            
                            Assert.IsTrue(GF.CheckCorrectText(PPattern.CartAmountSelector, "1"), "Товар успішно доданий в попап корзини");
                        }*/

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление двух одинаковых товаров, страница профиля, число в попапе пропало")]
        public void С3_DeleteIdenticalProfileCartAmountChange()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация ");
            PProf.DeleteAllItemsFromCartProfile("Очистка корзины");
            GF.NavigateToUrl(PCatalog.NovinkiUrl, "Перехожу в Новинки");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            GF.ClickOn(PPattern.CartCloseButton);
            GF.ClickOn(GXP.ItemSizeSecondOfList, "Выбираю другой размер");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попапа корзины");
            GF.NavigateToUrl(PProf.Url, "Перехожу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду отображение меню Профиля");
            GF.ClickOn(PProf.CartProfileButton, "В профиле нажимаю на корзину");
            GF.MoveToElement(PProf.MakeOrderButtonCartPageProfile);
            GF.ClickOn(PProf.DeleteFromCartProfileButton, "Удаляю товары с корзины");

            Thread.Sleep(1000);
            Assert.IsTrue(PPattern.CartAmountEmpty.Displayed, "Счетчик попапа сбросился");
        }

        //[Test]
        //[Author("Dmytro Lytovchenko"), Description("Изменение числа попапа - удаление 1 из 2 чекаут")]
        //public void С4_DeleteCheckoutOneCartAmountChange()
        //{
        //    GF.NavigateToUrl(PCatalog.SaleUrl);
        //    GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
        //    GF.ClickOn(GXP.ItemSize, "Выбираю размер");
        //    GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
        //    GF.Back();
        //    GF.ClickOn(PCatalog.ProductItemList[1], "Нажимаю на второй товар");
        //    GF.ClickOn(GXP.ItemSize, "Выбираю размер");
        //    GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
        //    GF.NavigateToUrl(PCheck.Url);
        //    GF.CheckAction(PCheck.DeleteItemFromCheckOutButton.Displayed, "Жду меню чекаута");
        //    PCheck.DeleteElementsCheckout("Удаляю все товары на чекауте, кроме", 1);
        //    Assert.IsTrue(GF.CheckCorrectText(PPattern.CartAmountSelector, "1"), "Иконка корзины отображает наличие 1го  товаров");
        //}

/*        [Test]
        [Author("Dmytro Lytovchenko"), Description("Изменение числа попапа - удаление 1 из 2 корзина")]
        public void С4_DeleteOneBasketCartAmountChange()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация");
            PProf.DeleteAllItemsFromCartProfile("Очистка корзины");
            GF.NavigateToUrl(PCatalog.BotinkiSumkiUrl);
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.NavigateToUrl(PCatalog.PolubotinkiUrl);
            GF.ClickOn(PCatalog.ProductItemList[1], "Нажимаю на второй товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.NavigateToUrl(PCheck.UrlBasket);
            GF.CheckAction(PCart.DeleteItemButton.Displayed);
            GF.ClickOn(PCart.DeleteItemButton, "");
            Assert.IsTrue(GF.CheckCorrectText(PPattern.CartAmountSelector, "1"), "Иконка корзины отображает наличие 1го  товаров");
        }*/

        /*        [Test]
                [Author("Dmytro Lytovchenko"), Description("Изменение числа попапа - очистка два одинаковых")]
                public void С8_ClearBasketCartAmountChange()
                {

                    GF.NavigateToUrl(PCatalog.BazoviyPlatyaUrl);
                    GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
                    GF.ClickOn(GXP.ItemSize, "Выбираю размер");
                    GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
                    GF.NavigateToUrl(PCheck.UrlBasket);
                    GF.ClickOn(PCheck.SelectSize(PCheck.DropDownQuantity, 1, "Увеличиваю количество до 2х товаров"));
                    Thread.Sleep(2000);
                    PCart.DeleteAllItemsFromCartAuth("Очистка корзины");
                    Assert.IsTrue(GF.CheckElementNotExist(PPattern.CartAmountEmpty), " Числа попап изменилось");
                }*/

/*        [Test]
        [Author("Dmytro Lytovchenko"), Description("Восстановление товара - попап изменился")]
        public void С5_RestoreBasketCartAmountChange()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация");
            PProf.DeleteAllItemsFromCartProfile("Очистка корзины");
            GF.NavigateToUrl(PCatalog.SaleUrl);
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.NavigateToUrl(PCheck.NovinkiUrl);
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.NavigateToUrl(PCheck.UrlBasket);
            GF.ClickOn(PCart.DeleteItemButton, "");
            Thread.Sleep(2000);
            GF.CheckAction(PCart.ReestablishItemButton.Displayed, "Восттановление");
            var before = PPattern.CartAmount.Text;
            GF.ClickOn(PCart.ReestablishItemButton, "Восстанавливаю");
            Thread.Sleep(2000);
            var after = PPattern.CartAmount.Text;
            Assert.AreNotEqual(before, after, "Числа попап изменилось");
        }*/

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Возвращения на пердыдущую страницу счетчик сохраняет значение ")]
        public void С6_PopupSaveValueAfterBack()
        {
            GF.NavigateToUrl(PCheck.BazJacketeUrl, "Базовые жакеты");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            GF.Back();
            GF.CheckAction(PCatalog.ProductItemList[0].Displayed);

            Assert.IsTrue(PPattern.GetQuantityCartPopup("1"), "Иконка корзины отображает наличие добавленного товара, при возвращении на предыдущую страницу ");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Возвращения на пердыдущую страницу кнопкой Назад, счетчик сохраняет значение")]
        public void С7_PopupSaveValueAfterButtonBack()
        {
            GF.NavigateToUrl(PCheck.BazJacketeUrl, "Базовые жакеты");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            GF.ClickOn(PPattern.CartCloseButton);
            GF.MoveToElementAndClick(PItem.BackButton,"Нажимаю кнопку Назад");
            GF.CheckAction(PCatalog.ProductItemList[0].Displayed);
            Assert.IsTrue(PPattern.GetQuantityCartPopup("1"), "Иконка корзины отображает наличие добавленного товара, при нажатии кнопки Назад");
        }
    }
}
