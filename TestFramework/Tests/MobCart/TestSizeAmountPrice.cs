﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using MobTestFramework.General;
using MobTestFramework.Pages;

namespace MobTestFramework.Tests.MobCart
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "C")]
    class TestSizeAmountPrice<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка выбраного размера в корзине")]
        public void C16_CheckSelectedSizeInCartPage()
        {            
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenBazovyyOfusSubMenuButton, "Раскрываю Базовый офис");
            GF.ClickOn(PPattern.BazovyePlatyaButton, "Нажимаю на Базовые платья");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к первому товару");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            var sizeText = GF.GetTextOfElement(GXP.ItemSize);//запись размера товара
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton);
            var sizeTextInCart = GF.GetTextOfElement(PCart.SizeOfItem);//запись размера товара
            Assert.IsTrue(sizeTextInCart.Contains(sizeText), "Размер в корзине соответствует выбраному");
        }

/*        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка выбраного количества в корзине")]
        public void C17_CheckSelectedQuantityInCartPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");           
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю Каталог");
            GF.ClickOn(PPattern.BluzyITunikiButton, "Выбираю Блузы и Туники");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к первому товару");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.ClickOn(PPattern.CartCloseButton);
            GF.ClickOn(GXP.ItemSizeSecondOfList, "Выбираю размер");
            GF.WaitForClickabilityOfElement(GXP.BuyButton, 10);
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton);
            Assert.IsTrue(PCart.QuantityOfItem.Text.Contains("2"), "Количество товаров в корзине верно");
        }*/

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка правильности отображения цены в корзине")]
        public void C18_CheckCorrectnesOfPriceInCartPage()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.NovyePostupleniyaButton, "Выбираю Новинки");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            var priceText = GF.GetTextOfElement(PItem.PriceOfItem);//запись цены товара
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton);
            Assert.IsTrue(GF.GetTextOfElement(PCart.PriceOfItem).Contains(priceText), "Цена отображена верно");
        }
    }
}
