﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using MobTestFramework.General;
using MobTestFramework.Pages;

namespace MobTestFramework.Tests.CheckOut
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Чекаут"), Property("TestSuite:", "CH")]
    class TestCheckOut<TBrowser> : TestBase<TBrowser>
    {
        private readonly string EmailFacebook = "facebookqaaniart@gmail.com";

        private readonly string EmptyCartMessage = "В корзине пусто";

        private readonly string Street = "Аниартовская";
        private readonly string Home = "4/6";
        private readonly string Apartment = "5";

        private readonly string OfficeDelivery = "50";
        private readonly string HomeDelivery = "80";


        [Test]
        [Author("Dmytro Lytovchenko"), Description("Авторизация на странице чекаута")]
        public void CH1_AuthorizationCheckoutPage()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю на Sale");
            GF.ClickOn(PPattern.ZhaketyIZhiletyButton, "Нажимаю на Жакеты и жилеты");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Оформить покупку");
            GF.SendKeys(PCheck.LoginUserInput, User.UserFirst.Email, "Вводим логин");
            GF.SendKeys(PCheck.PasswordUserInput, User.UserFirst.Password, "Вводим пароль");
            GF.ClickOn(PCheck.AuthSubmitButton, "Нажимаю кнопку авторизации");
            Assert.IsTrue(PCheck.FinishCheckoutButton.Displayed, "Успешная авторизация на странице чекаута");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка корректности расчёта доставки на чекауте, доставка бесплатная")]
        public void CH2_FreeDeliveryCheckoutPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.NovyePostupleniyaButton, "Нажимаю на Новинки");

            //filter
            var old = PCatalog.ProductItemList[0];
            GF.ClickOn(PCatalog.OpenSortFilterButton, "Раскрываю фильтр сортировки");
            GF.ClickOn(PCatalog.UbivanieSelectRight, "Выбираю сортировку Цена по Убыванию");
            GF.CheckElementNotAttachedToDom(old);

            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");

            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            GF.ClickOn(PPattern.CartCloseButton);

            GF.ClickOn(GXP.ItemSizeSecondOfList, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");

            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Оформить покупку");
            Assert.IsTrue(PCheck.FreeDeliveryMessageCheckout.Displayed, "Сообщение о бесплатной доставке отображено");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка корректности расчёта доставки на чекауте, доставка платная в отделение")]
        public void CH3_PaidOfficeDeliveryCheckoutPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация ");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.NovyePostupleniyaButton, "Нажимаю на Новинки");

            //filter
            var old = PCatalog.ProductItemList[0];
            GF.ClickOn(PCatalog.OpenSortFilterButton, "Раскрываю фильтр сортировки");
            GF.ClickOn(PCatalog.VozrastanieSelectRight, "Выбираю сортировку Цена по Возрастанию");
            GF.CheckElementNotAttachedToDom(old);

            //Thread.Sleep(1000);
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Оформить покупку");
            Assert.AreEqual(PCheck.PaidDeliveryMessageCheckoutOfomlenie.Displayed, PCheck.PaidDeliveryMessageCheckoutOfomlenie.Text.Contains(OfficeDelivery), "Повідомлення про доставку висвітлено");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление всех кроме одного товара на чекауте, этап Оформление заказа, пользователь не авторизован")]
        public void CH4_DeletePartOfItemFromCartCheckoutPage()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю каталог");
            GF.ClickOn(PPattern.PlatyaButton, "нажимаю на Платья");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.Back();
            GF.ClickOn(PCatalog.ProductItemList[1], "Нажимаю на второй товар");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");          
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");
            GF.ClickOn(PPattern.GoToCartButton, "в попапе нажимаю на кнопку Оформить покупку");
            GF.CheckAction(PCheck.ShowProductsButton.Displayed, "жду меню чекаута");
            GF.ClickOn(PCheck.ShowProductsButton, "Раскрываю список товаров");
            Thread.Sleep(1000);
            var beforeDeleting = PCheck.ProductsInCheckout.Count;
            PCheck.DeleteElementsCheckout("удаляю все товары на чекауте, кроме", 1);
            var afterDeleting = PCheck.ProductsInCheckout.Count;
            Assert.AreNotEqual(afterDeleting, beforeDeleting, "Количество товаров на чекауте уменьшилась");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Авторизация на странице чекаута через фейсбук")]
        public void CH5_AuthorizationFacebookCheckoutPage()
        {
            GF.NavigateToUrl(PCatalog.PlatyaUrl, "Перехожу на страницу Платья");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к товару");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю розмер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.NavigateToUrl(PCheck.Url, "Перехожу на страницу Чекаута");
            GF.ClickOn(PCheck.FacebookCheckoutAutr, "Нажимаю кнопку авторизации Facebook");
            PAthrzt.FacebookAuth(User.UserFacebook, "Выполняю авторизацию");
            Assert.IsTrue(PCheck.EmailUserInput.GetAttribute("value").Equals(EmailFacebook), "Авторизация выполнена");
            PCart.DeleteAllItemsFromBasket("Очистка корзины");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление одного товара на чекауте, пользователь авторизован")]
        public void CH6_DeleteOneItemFromCartCheckoutPageAuthorizedUser()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация ");
            PProf.DeleteAllItemsFromCartProfile("Очистка корзины");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.NovyePostupleniyaButton, "нажимаю на Новинки");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.ClickOn(PPattern.GoToCartButton, "в попапе нажимаю на кнопку Оформить покупку");
            GF.CheckAction(PCheck.ShowProductsButton.Displayed, "жду меню чекаута");
            PCheck.DeleteElementsCheckout("удаляю все товары на чекауте");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду отображения меню");
            GF.ClickOn(PProf.CartProfileButton, "нажимаю на подпункт корзина");
            Assert.IsTrue(GF.CheckCorrectText(PProf.ProfileMessageOfEmptyCart, EmptyCartMessage), "Корзина пуста");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление двух товаров на чекауте, пользователь авторизован")]
        public void CH7_DeleteTwoItemFromCartCheckoutPageAuthorizedUser()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация ");
            PProf.DeleteAllItemsFromCartProfile("Очистка корзины");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.NovyePostupleniyaButton, "нажимаю на Новинки");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.SaleButton, "нажимаю на Sale");
            GF.ClickOn(PCatalog.ProductItemList[1], "Выбираю второй товар");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.ClickOn(PPattern.GoToCartButton, "в попапе нажимаю на кнопку Оформить покупку");
            GF.CheckAction(PCheck.ShowProductsButton.Displayed, "жду меню чекаута");
            PCheck.DeleteElementsCheckout("удаляю все товары на чекауте");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду отображения меню");
            GF.ClickOn(PProf.CartProfileButton, "нажимаю на подпункт корзина");
            Assert.IsTrue(GF.CheckCorrectText(PProf.ProfileMessageOfEmptyCart, EmptyCartMessage), "Корзина пуста");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Почтовые адреса меняются в соответствии с городом")]
        public void CH8_PostalAddressesChangeAccordingToCity()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.NovyePostupleniyaButton, "нажимаю на Новинки");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");
            GF.ClickOn(PPattern.GoToCartButton, "в попапе нажимаю на кнопку Оформить покупку");
            GF.CheckAction(PCheck.FirstBuyButton.Displayed, "Жду загрузки страницы чекаута");
            GF.ClickOn(PCheck.FirstBuyButton, "Нажимаю на кнопку Покупаю впервые");
            GF.MoveToElement(PCheck.InputCity, "Направляюсь к полям ввода");
            GF.DropDownClick(PCheck.DropDownWayOfDelivery, 0, "Доставка в отделение");
            GF.SendKeys(PCheck.InputCity, "Львов", "Ввожу город");
            Thread.Sleep(1000);
            GF.ClickOn(PCheck.ChoosePostOfficeButton, "Открываю список отделений");
            List<string> firstCityPost = PCheck.DropDown(PCheck.DropDownPost);
            GF.SendKeys(PCheck.InputCity, "", "Очищаю поле ввода");
            Thread.Sleep(1000);
            GF.SendKeys(PCheck.InputCity, "ЧЕРКАССЫ", "Ввожу город");
            Thread.Sleep(1000);
            GF.MoveToElementAndClick(PCheck.Podskazka, "Открываю список отделений");
            List<string> secondtCityPost = PCheck.DropDown(PCheck.DropDownPost);
            Assert.IsFalse(PCheck.ComparingTwoListsBeElements(firstCityPost, secondtCityPost), "Отделения изменились");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка функционал заказа товара до двери ")]
        public void CH9_EnterHomeAddress()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.SaleButton, "нажимаю на Распродажу");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");
            GF.ClickOn(PPattern.GoToCartButton, "в попапе нажимаю на кнопку Оформить покупку");
            GF.CheckAction(PCheck.FirstBuyButton.Displayed, "Жду загрузки страницы чекаута");
            GF.SendKeys(PCheck.LoginUserInput, User.UserFirst.Email, "Ввожу логин");
            GF.SendKeys(PCheck.PasswordUserInput, User.UserFirst.Password, "Ввожу пароль");
            GF.ClickOn(PCheck.AuthSubmitButton, "Авторизация");
            Thread.Sleep(2000);
            GF.DropDownClick(PCheck.DropDownWayOfDelivery, 1, "Доставка к дому");
            PCheck.FillInputFields(Street, Home, Apartment);
            GF.MoveToElementAndClick(PCheck.CartCheckoutButton, "Перехожу в корзину");
            GF.ClickOn(PCheck.OrderCheckoutButton, "Перехожу в Оформление заказа");
            Assert.IsTrue(PCheck.TextWasInput(Street, Home, Apartment), "Заказ до двери доступен");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Выбранное отделение почты отображается")]
        public void CH10_SelectedPostOfficeAdressApplyed()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.NovyePostupleniyaButton, "нажимаю на Новинки");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");
            GF.ClickOn(PPattern.GoToCartButton, "в попапе нажимаю на кнопку Оформить покупку");
            GF.CheckAction(PCheck.FirstBuyButton.Displayed, "Жду загрузки страницы чекаута");
            GF.ClickOn(PCheck.FirstBuyButton, "Нажимаю на кнопку Покупаю впервые");
            GF.DropDownClick(PCheck.DropDownWayOfDelivery, 0, "Доставка в отделение");
            Thread.Sleep(2000);
            GF.SendKeys(PCheck.InputCity, "", "Очищаю поле ввода");
            Thread.Sleep(1000);
            GF.SendKeys(PCheck.InputCity, "ЧЕРКАССЫ", "Ввожу город");
            Thread.Sleep(1000);
            Assert.IsTrue(PCheck.PostIsDisplayed(), "Выбранный адрес почты отображается");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка функционала кнопок способов оплаты")]
        public void CH11_CheckboxPaymentOptions()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация ");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю Каталог");
            GF.ClickOn(PPattern.AksessuaryButton, "нажимаю на Аксессуары");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");
            GF.ClickOn(PPattern.GoToCartButton, "в попапе нажимаю на кнопку Оформить покупку");
            Thread.Sleep(1000);
            GF.CheckAction(PCheck.GeneralPrice.Displayed, "Жду загрузки страницы чекаута");
            Assert.IsTrue(PCheck.ChangePayment(false, "переключаю на оплату картой"), "RadiоButton оплаты меняются в зависимости от выбора");
            Assert.IsTrue(PCheck.ChangePayment(true, "переключаю на НАЛОЖЕННЫЙ ПЛАТЕЖ"), "RadiоButton оплаты меняются в зависимости от выбора");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка корректности расчёта суммы на Чекауте")]
        public void CH12_CheckSummOfTwoItemsCheckOutPage()
        {
            GF.NavigateToUrl(PPattern.NovinkiUrl);
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");
            GF.NavigateToUrl(PPattern.BazJacketeUrl);
            GF.ClickOn(PCatalog.ProductItemList[1], "Выбираю второй товар");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");
            GF.ClickOn(PPattern.GoToCartButton, "в попапе нажимаю на кнопку Оформить покупку");
            GF.CheckAction(PCheck.CheckoutFeild.Displayed, "Жду отображение чекаута");
            GF.ClickOn(PCheck.ShowProductsButton, "Раскрываю список товаров");
            var final = (PCheck.GetNumberPriceRegex(PCheck.PriceOfFirstItem) + PCheck.GetNumberPriceRegex(PCheck.PriceOfSecondItem));
            Assert.AreEqual(final, PCheck.GetNumberPriceRegex(PCheck.GeneralPrice), "Подсчет верный");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка корректности расчёта доставки на чекауте, доставка платная к дому")]
        public void CH13_PaidHomeDeliveryCheckoutPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация ");
            PProf.DeleteAllItemsFromCartProfile("Удаление всех товаров с корзины");
            GF.NavigateToUrl(PPattern.NovinkiUrl);
            GF.CheckAction(PCatalog.ProductItemList[0].Displayed, "Жду загрузки каталога");

            //filter
            var old = PCatalog.ProductItemList[0];
            GF.ClickOn(PCatalog.OpenSortFilterButton, "Раскрываю фильтр сортировки");
            GF.ClickOn(PCatalog.UbivanieSelectRight, "Выбираю сортировку Цена по Возрастанию");
            GF.CheckElementNotAttachedToDom(old);

            Thread.Sleep(2000);//wait for refreshing
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");
            GF.ClickOn(PPattern.GoToCartButton, "в попапе нажимаю Оформить покупку");
            GF.ClickOn(PCheck.GeneralPrice, "Нажимаю покупаю впервые");
            GF.DropDownClick(PCheck.DropDownWayOfDelivery, 1, "Выбираю досавку ");
            Thread.Sleep(2000);
            Assert.AreEqual(PCheck.PaidDeliveryMessageCheckoutOfomlenie.Displayed, PCheck.PaidDeliveryMessageCheckoutOfomlenie.Text.Contains(HomeDelivery), "Сообщение о доставка отобразилось");
        }


        /*        [Test]
        [Category("Dmytro Lytovchenko"), Description("Удаление одного товара на чекауте, этап Оформление заказа, пользователь не авторизован")]
        public void DeleteOneItemFromCartCheckoutPage()
        {
            GF.MoveToElement(PPattern.ObuvSumkiButton, "направляюсь к Обувь и сумки");
            GF.ClickOn(PPattern.SumkiButton, "нажимаю на Сумки");
            GF.MoveToElement(PCatalog.ProductItem, "направляюсь к товару");
            GF.ClickOn(PCatalog.QuickViewOfFirstItem, "нажимаю на Быстрый просмотр первого товара");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.ClickOn(PPattern.MakeOrderButton, "в попапе нажимаю на кнопку Оформить покупку");
            PCheck.DeleteElementsCheckout("удаляю все товары на чекауте");      
            Assert.IsTrue(PProf.UnregistredUserMessage.Displayed, "Товар удален");
        }*/

        /*        [Test]
                [Category("Dmytro Lytovchenko"), Description("Удаление двух товаров на чекауте, этап Оформление заказа, пользователь не авторизован")]
                public void DeleteTwoItemFromCartCheckoutPage()
                {
                    GF.MoveToElement(PPattern.CatalogButton, "направляюсь к Каталогу");
                    GF.ClickOn(PPattern.PlatyaButton, "нажимаю на Платья");
                    GF.MoveToElement(PCatalog.ProductItem, "направляюсь к товару");
                    GF.ClickOn(PCatalog.QuickViewOfFirstItem, "нажимаю на Быстрый просмотр первого товара");
                    GF.ClickOn(GXP.ItemSize, "выбираю размер");
                    GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
                    GF.ClickOn(GXP.ItemSizeSecondOfList, "выбираю другой размер");
                    GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
                    GF.ClickOn(PPattern.MakeOrderButton, "в попапе нажимаю на кнопку Оформить покупку");
                    GF.CheckAction(PCheck.DeleteItemFromCheckOutButton.Displayed, "жду меню чекаута");
                    PCheck.DeleteElementsCheckout("удаляю все товары на чекауте");
                    Assert.IsTrue(PProf.UnregistredUserMessage.Displayed, "Товар удален");
                }*/

        /*        [Test]
                [Category("Dmytro Lytovchenko"), Description("Удаление всех кроме одного товара на чекауте, этап Оформление заказа, пользователь не авторизован")]
                public void DeletePartOfItemFromCartCheckoutPage()
                {
                    GF.MoveToElement(PPattern.CatalogButton, "направляюсь к Каталогу");
                    GF.ClickOn(PPattern.PlatyaButton, "нажимаю на Платья");
                    GF.MoveToElement(PCatalog.ProductItem, "направляюсь к товару");
                    GF.ClickOn(PCatalog.QuickViewOfFirstItem, "нажимаю на Быстрый просмотр первого товара");
                    GF.ClickOn(GXP.ItemSize, "выбираю размер");
                    GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
                    GF.ClickOn(GXP.ItemSizeSecondOfList, "выбираю другой размер");
                    GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
                    GF.ClickOn(PPattern.MakeOrderButton, "в попапе нажимаю на кнопку Оформить покупку");
                    GF.CheckAction(PCheck.DeleteItemFromCheckOutButton.Displayed, "жду меню чекаута");
                    PCheck.DeleteElementsCheckout("удаляю все товары на чекауте");
                    Assert.IsTrue(PProf.CartPrifileTitleZero.Text.Contains(EmptyCartMessage), "Товар удален");
                }*/
    }
}