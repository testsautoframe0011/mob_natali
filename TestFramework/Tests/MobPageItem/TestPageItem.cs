﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using MobTestFramework.General;
using MobTestFramework.Pages;

namespace MobTestFramework.Tests.MobPageItem
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Страница товара"), Property("TestSuite:", "PI")]
    class TestPageItem<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Смена цвета товара на странице товара")]
        public void PI1_ChangeColourOfItemInItemPage()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Разворачиваю Каталог");
            GF.ClickOn(PPattern.VerkhnyayaOdezhdaButton, "Нажимаю на Верхнюю одежду");
            GF.ClickOn(PCatalog.ItemWithFewColors[0], "Выбираю первый товар");
            var title = GF.GetTextOfElement(PItem.NameOfItemInPageItem).Trim();
            GF.ClickOn(GXP.ChangeColour, "Меняю цвет товара");
            Thread.Sleep(1000);//wait for loading
            Assert.AreNotEqual(GF.GetTextOfElement(PItem.NameOfItemInPageItem).Trim(), title, "Цвет изменен");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Смена типа размеров на странице товара")]
        public void PI2_ChangeTypeOfItemSizesInItemPage()
        {
            GF.NavigateToUrl(PPattern.SaleUrl);
            GF.ClickOn(PCatalog.ProductItemList[0]);
            List<string> strL = PItem.MakeNewList(PItem.SizesPageItemList);//запись размеров
            GF.ClickOn(PItem.ChangeTypeOfSizesButton, "Меняю тип размеров");
            List<string> strL1 = PItem.MakeNewList(PItem.SizesPageItemList);//Запись размеров
            Assert.IsFalse(PItem.ComparingTwoLists(strL, strL1), "Тип размеров изменился");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка наличия таблицы размеров")]
        public void PI3_CheckSizeTableInPageItem()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Разворачиваю Каталог");
            GF.ClickOn(PPattern.KostyumyButton, "Нажимаю на Костюмы");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(PItem.SizeTableButton, "Нажимаю на Таблицу розмеров");
            Assert.IsTrue(PItem.SizeTablePopup.Displayed, "Таблицу розмеров отображена");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Просмотр наличия товара в магазинах")]
        public void PI4_ViewItemAvailabilityInStore()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.NovyePostupleniyaButton, "Выбираю Новинки");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к первому товару");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(PItem.AvailabilityInStore, "нажимаю на Наличие в магазинах");
            //Thread.Sleep(2000);
            Assert.IsTrue(PItem.CheckTheDifferenceInStoreAddresses(), "Адреса магазинов отображаются");
        }
    }
}
