﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using MobTestFramework.General;
using MobTestFramework.Pages;
using System.Threading;

namespace MobTestFramework.Tests
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Профиль"), Property("TestSuite:", "P")]
    class TestProfile<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Выход из аккаунта")]
        public void P1_LogOutFromAccount()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PPattern.EnterProfileByClickOnIcon("Перехожу в Профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля",15);
            GF.ClickOn(PProf.LogOutButton, "Нажимаю на выход с Профиля");
            Thread.Sleep(2000);
            PPattern.EnterProfileByClickOnIcon("Переход в профиль");
            Assert.IsTrue(PAthrzt.AuthorizationPopup.Displayed, "Пользователь вышел, попап авторизации отобразился");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Переход на страницу товара через лист желаний профиль")]
        public void P2_GoToItemPageFromFishListProfile()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromWishListAuth("Удаление всех товаров с листа желаний");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenObuvISumkiSubMenuButton, "Раскрываю Обувь и ботинки");
            GF.ClickOn(PPattern.BotinkiButton, "Выбираю Полуботинки");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к первому товару");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "Добавляю товар в лист желаний");
            PPattern.EnterProfileByClickOnIcon("Перехожу в Профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "Нажимаю на Лист желаний");
            var title = GF.GetTextOfElement(PProf.TitleOfFirstItemWishList);
            GF.ClickOn(PProf.FirstItemInWishList, "Перехожу на страницу товара");
            Assert.AreEqual(GF.GetTextOfElement(PItem.NameOfItemInPageItem), title, "Успешно перешли на страницу товара");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Возможность изменить личные данные в профиле")]
        public void P3_ChangePersonalInformationProfile()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PPattern.EnterProfileByClickOnIcon("Перехожу в Профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            Thread.Sleep(2000);
            GF.ClickOn(PProf.PersonalDataButton, "Нажимаю на Личные данные");
            var name = PProf.NameInput.GetAttribute("value");
            var surname = PProf.LastNameInput.GetAttribute("value");
            GF.SendKeys(PProf.NameInput, surname, "Меняю Имя");
            GF.SendKeys(PProf.LastNameInput, name, "Меняю Фамилию");
            GF.ClickOn(PProf.SavePersonalInfo, "Сохраняю изменения");
            Assert.IsTrue(PProf.SuccessMessage.Displayed, "Уведомление об успешных изменениях отображено");
        }
    }
}
