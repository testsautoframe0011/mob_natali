﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;
using MobTestFramework.General;
using MobTestFramework.Pages;
using System.Threading;
using System.Collections.Generic;
using System;

namespace MobTestFramework.Tests.Filter
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Тест Фильтр и Сортировка"), Property("TestSuite:", "F")]
    public class FilterSort<TBrowser> : TestBase<TBrowser>
    {
        private readonly string RedColour = "Красный";
        private readonly string BlackColour = "Черный";

        private readonly string InputMinPriceFilter = "1000";
        private readonly string InputMaxPriceFilter = "2000";

        [Test]
        [Author("Vanya Demenko"), Description("Проверка работы фильтра: Цвет")]
        public void F1_ColourFilterOnCatalogPageLeftMenu()
        {
            GF.NavigateToUrl(PCatalog.PlatyaUrl);
            GF.ClickOn(PCatalog.OpenFilterButton, "Открываю фильтр");
            Thread.Sleep(1000);
            GF.ClickOn(PCatalog.RedColourInFilterLeftMenu, "обераю червоний колір");
            Thread.Sleep(1000);
            GF.CheckAction(PCatalog.ResetAllFiltersButton.Displayed);
            GF.ClickOn(PCatalog.CloseFilterButton, "Закрываю фильтр ");

            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            Assert.AreEqual(GF.GetTextOfElement(PItem.ItemColor),RedColour, "Товар потрібного кольору");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка работы фильтра: Размер ")]
        public void F3_SizeFilterOnCatalogPageLeftMenu()
        {
            GF.NavigateToUrl(PCatalog.BluziTunikiUrl);

            GF.ClickOn(PCatalog.OpenFilterButton, "Открываю фильтр");
            var sizeOfFilter = GF.GetTextOfElement(PCatalog.FirstSizeInFilterLeftMenu);

            GF.ClickOn(PCatalog.FirstSizeInFilterLeftMenu, "обераю перший розмір зі списку доступних розмірів");
            Thread.Sleep(1000);
            GF.CheckAction(PCatalog.ResetAllFiltersButton.Displayed);
            GF.ClickOn(PCatalog.CloseFilterButton, "Закрываю фильтр ");

            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            Assert.AreEqual(GF.GetTextOfElement(GXP.ItemSize), sizeOfFilter, "Товар потрібного розміру");
        }

/*        [Test]
        [Author("Vanya Demenko"), Description("Проверка работы фильтра: Размеры eu/ua (Левое боковое меню фильтров)")]
        public void F5_TypeOfSizesFilterOnCatalogPageLeftMenu()
        {
            GF.NavigateToUrl(PCatalog.TopyiUrl);

            GF.MoveToElement(PCatalog.ProductItemList[0], "навівся на товар");
            GF.CheckAction(PCatalog.HoverSize.Displayed, "");//Thread.Sleep(4000);
            List<string> strL = PItem.MakeNewList(PCatalog.SizesHoverEffectList);//записую розміри в лист, при hover

            GF.ClickOn(PCatalog.ChangeTypeOfSizesButton, "змінюю тип розмірів");
            GF.MoveToElement(PCatalog.ProductItemList[0], "навівся на товар");

            GF.CheckAction(PCatalog.SizesHoverEffectList[0].Displayed);
            //Thread.Sleep(4000);
            List<string> strL1 = PItem.MakeNewList(PCatalog.SizesHoverEffectList);//записую розміри в лист, при hover
            Assert.IsFalse(PItem.ComparingTwoLists(strL, strL1), "Тип розмірів змінено");
        }*/


        [Test]
        [Author("Vanya Demenko"), Description("Проверка работы фильтра: Цена")] //ползунок цены
        public void F6_PriceFilterInputCatalog()
        {

            GF.NavigateToUrl(PCatalog.SaleUrl);
            GF.ClickOn(PCatalog.OpenFilterButton, "Открываю фильтр");
            GF.ClickOn(PCatalog.MoreFiltersButton, "");
            GF.ClickOn(PCatalog.OpenPriceFilter, "");

            GF.SendKeysClickEnter(PCatalog.PriceMinFilter, InputMinPriceFilter, "Минимальная цена 1000");
            Thread.Sleep(3000);
            GF.SendKeysClickEnter(PCatalog.PriceMaxFilter, InputMaxPriceFilter, "Максимальная цена 2000");
            Thread.Sleep(3000);
            GF.ClickOn(PCatalog.CloseFilterButton, "Закрываю фильтр ");
            Thread.Sleep(2000);

            Assert.IsTrue(PCatalog.CheckPriceFilterMy(PCatalog.ProductItemListPrice, InputMinPriceFilter, InputMaxPriceFilter), "Фільтр працює вірно");
        }


        [Test]
        [Author("Vanya Demenko"), Description("Возможность сбросить все фильтры на странице каталога")] 
        public void F8_ResetAllFiltersInputsOnCatalogPage()
        {
            GF.NavigateToUrl(PCatalog.PlatyaUrl);
            GF.ClickOn(PCatalog.OpenFilterButton, "Открываю фильтр");

            GF.ClickOn(PCatalog.RedColourInFilterLeftMenu, "обераю червоний колір");
            Thread.Sleep(1000);
            GF.CheckAction(PCatalog.ResetAllFiltersButton.Displayed);

            GF.ClickOn(PCatalog.SizeInFilterList[3], "обераю 4 розмір зі списку доступних розмірів");
            Thread.Sleep(1000);
            GF.CheckAction(PCatalog.ResetAllFiltersButton.Displayed);

            Thread.Sleep(2000);
            GF.ClickOn(PCatalog.CloseFilterButton, "Закрываю фильтр ");

            var title = GF.GetTextOfElement(PCatalog.ProductItemsTitlesList[0]);
            GF.ClickOn(PCatalog.OpenFilterButton, "Открываю фильтр");

            GF.ClickOn(PCatalog.ResetAllFiltersButton, "натискаю на Сбросить фильтр");
            Thread.Sleep(6000);
            GF.ClickOn(PCatalog.CloseFilterButton, "Закрываю фильтр ");

            Assert.AreNotEqual(GF.GetTextOfElement(PCatalog.ProductItemsTitlesList[0]), title, "Фільтр працює вірно");
        }


        [Test]
        [Author("Vanya Demenko"), Description("Отключение одной из фильтрации")] 
        public void F9_ResetAllFiltersInputsOnCatalogPage()
        {
            GF.NavigateToUrl(PPattern.PlatyaUrl, "иду на Платья");

            GF.ClickOn(PCatalog.OpenFilterButton, "Открываю фильтр");

            //filter
            GF.ClickOn(PCatalog.RedColourInFilterLeftMenu, "обераю червоний колір");
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);



            //filter
            GF.ClickOn(PCatalog.SizeInFilterList[1], "обераю другий розмір зі списку доступних розмірів");
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);

            Thread.Sleep(1000);
            GF.ClickOn(PCatalog.CloseFilterButton, "Закрываю фильтр ");
            Thread.Sleep(1000);

            var title = GF.GetTextOfElement(PCatalog.ProductItemsTitlesList[0], true);

            GF.ClickOn(PCatalog.OpenFilterButton, "Открываю фильтр");

            GF.ClickOn(PCatalog.ResetAllFiltersButton, "Отменяю выбор фильтров");
            Thread.Sleep(4000);
            GF.ClickOn(PCatalog.CloseFilterButton, "Закрываю фильтр ");
            Assert.AreNotEqual(GF.GetTextOfElement(PCatalog.ProductItemsTitlesList[0], true), title, "Фільтр працює вірно");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Фильтрованные товары сортированы верно")]
        public void ST1_SortAfterFilter()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Раскрываю меню");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю меню");
            GF.ClickOn(PPattern.PlatyaButton, "натискаю на костюми");
            GF.CheckAction(PCatalog.ProductItemList[1].Displayed);

            //filter
            GF.ClickOn(PCatalog.OpenFilterButton, "Раскріваю фильтр");
            GF.ClickOn(PCatalog.RedColourInFilterLeftMenu, "обераю червоний колір");
            GF.ClickOn(PCatalog.CloseFilterButton, "Закрываю фильтр");

            //filter
            Thread.Sleep(3000);
            var sort = PCatalog.ProductItemsTitlesList;

            GF.ClickOn(PCatalog.CatalogSortSelectRight,"Раскрываю Типсортировки");
            GF.ClickOn(PCatalog.UbivanieSelectRight,"Сортирую по убыванию");


            Assert.IsTrue(PCatalog.SortPrice2(PCatalog.ResultsBySortProducts, false), "Сторінка відсортована вірно");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Сортированые товары фильтрованы согласно сортировке")]
        public void ST2_SortAfterFilterOff()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Раскрываю меню");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю меню");
            GF.ClickOn(PPattern.PlatyaButton, "натискаю на костюми");
            GF.CheckAction(PCatalog.ProductItemList[0].Displayed);

            //filter
            var old = PCatalog.ProductItemsTitlesList[0].Text;
            GF.ClickOn(PCatalog.CatalogSortSelectRight, "Раскрываю сортировку");
            GF.ClickOn(PCatalog.VozrastanieSelectRight, "Сортирую по возрастанию");
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(PCatalog.SortPrice2(PCatalog.ResultsBySortProducts, true));

            //filter
            GF.ClickOn(PCatalog.OpenFilterButton, "Раскрываю фильтр");
            GF.ClickOn(PCatalog.RedColourInFilterLeftMenu, "обераю червоний колір");
            GF.ClickOn(PCatalog.CloseFilterButton, "Закрываю фильтр");
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            Thread.Sleep(1000);

            Assert.IsTrue(PCatalog.SortPrice2(PCatalog.ResultsBySortProducts, true), "Фильтрация не бивает сортировку");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Сортировка не сбивается после отмені фильтрации")]
        public void ST3_SortAfterFilter()
        {

            GF.NavigateToUrl(PPattern.PlatyaUrl, "натискаю на костюми");
            GF.CheckAction(PCatalog.ProductItemList[1].Displayed);

            //filter
            GF.ClickOn(PCatalog.OpenFilterButton, "Раскріваю фильтр");
            GF.ClickOn(PCatalog.BlackColourInFilterLeftMenu, "обераю червоний колір");
            Thread.Sleep(2000);
            GF.ClickOn(PCatalog.CloseFilterButton, "Закрываю фильтр");

            //filter
            Thread.Sleep(1000);
            var sort = PCatalog.ProductItemsTitlesList;

            GF.ClickOn(PCatalog.CatalogSortSelectRight, "Раскріваю Типсортировки");
            GF.ClickOn(PCatalog.UbivanieSelectRight, "Сортирую по убыванию");

            Thread.Sleep(2000);
            var filtersort = PCatalog.ProductItemsTitlesList;

            GF.CheckAction(PCatalog.SortPrice2(PCatalog.ResultsBySortProducts, false));

            //filter
            GF.ClickOn(PCatalog.OpenFilterButton, "Раскрываю фильтр");
            GF.ClickOn(PCatalog.DeselectColor, "Отменяю фильтрацию по цене");
            Thread.Sleep(2000);
            GF.ClickOn(PCatalog.CloseFilterButton, "Закрываю фильтр");
            

            Assert.IsTrue(PCatalog.SortPrice2(PCatalog.ResultsBySortProducts, false), "Сторінка відсортована вірно");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Фильтрация не сбивается после изменения сортировки ")]
        public void ST4_SortAfterFilterOff()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Раскрываю меню");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю меню");
            GF.ClickOn(PPattern.PlatyaButton, "натискаю на костюми");
            GF.CheckAction(PCatalog.ProductItemList[0].Displayed);

            //filter
            GF.ClickOn(PCatalog.CatalogSortSelectRight, "Раскрываю сортировку");
            GF.ClickOn(PCatalog.VozrastanieSelectRight, "Сортирую по возрастанию");
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(PCatalog.SortPrice2(PCatalog.ResultsBySortProducts, true));

            //filter
            GF.ClickOn(PCatalog.OpenFilterButton, "Раскрываю фильтр");
            GF.ClickOn(PCatalog.RedColourInFilterLeftMenu, "обераю червоний колір");
            GF.ClickOn(PCatalog.CloseFilterButton, "Закрываю фильтр");
            Thread.Sleep(1000);
            var count = PCatalog.ProductItemList.Count;

            //filter
            GF.ClickOn(PCatalog.CatalogSortSelectRight, "Раскрываю сортировку");
            GF.ClickOn(PCatalog.NoninkiSelectRight, "Сортирую по Новинкам");
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            Thread.Sleep(2000);
            Assert.AreEqual(count,PCatalog.ProductItemList.Count, "Сортировка не отменяет филтьтрацию");           
        }
    }
}
