﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Text;
using MobTestFramework.General;
using MobTestFramework.Pages;
using MobTestFramework.Pages.PagesNashyTrendy;
using TestFramework.Pages.PagesNashyTrendy;
using TestFramework.Pages;
using TestFramework.General;

namespace MobTestFramework.Tests
{

    public class TestBase<TBrowser>
    {
        public IWebDriver Driver { get => Drivers.dr; }
        public string baseUrl { get => Config.BaseUrl; }

        [SetUp]
        public void CreateDriver()
        {
            if (typeof(TBrowser) == typeof(ChromeDriver)) { new Drivers("ch"); }

            if (typeof(TBrowser) == typeof(FirefoxDriver)) { new Drivers("fr"); }

            if (typeof(TBrowser) == typeof(string)) { new Drivers("ch-hd"); }
            Driver.Navigate().GoToUrl(Config.BaseUrl);
            PageBase.WaitPageFullLoaded();

        }

        [TearDown]
        public void TeardownTest()
        {
            Driver.Close();
            Driver.Quit();
        }


        public PageBase PPattern = new PageBase();
        public GeneralXPath GXP = new GeneralXPath();
        public PageCatalog PCatalog = new PageCatalog();
        public User PAthrzt = new User();
        public PageProfile PProf = new PageProfile();
        public PageCart PCart = new PageCart();
        public PageItem PItem = new PageItem();
        public PageCheckOut PCheck = new PageCheckOut();
        public PageShops PShop = new PageShops();


        //Nashi Trendy
        public PageNashiTkani PNashiTkany = new PageNashiTkani();
        public PageStayWarm PStayWarm = new PageStayWarm();
        public PageOfficeCollection PV21 = new PageOfficeCollection();
    }
}
