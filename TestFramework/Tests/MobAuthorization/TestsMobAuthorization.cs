﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;
using MobTestFramework.General;
using MobTestFramework.Pages;

namespace MobTestFramework.Tests.TestAuthorization
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Авторизация"), Property("TestSuite:", "A")]
    class TestAuthorization<TBrowser> : TestBase<TBrowser>
    {
        private readonly string EXPECTED_AUTHORIZATION_URL = "personal";

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Авторизация клиента"),]
        public void A1_AuthorizationUser()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PPattern.EnterProfileByClickOnIcon("Переход в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            Assert.IsTrue(Driver.Url.Contains(EXPECTED_AUTHORIZATION_URL), "Пользователь авторизован");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Авторизация клиента с неправильным логином")]
        public void A2_AuthorizationUserWithWrongLogin()
        {
            PProf.EnterProfileByClickOnIcon("Нажимаю на иконку профиля");
            GF.SendKeys(PAthrzt.EmailInput, "aniartqa@gmail.com", "вводим логин");
            GF.SendKeys(PAthrzt.PasswordInput, "32424243423", "Ввожу некорректный пароль");
            GF.ClickOn(PAthrzt.SubmitButton, "Нажимаю Войти");
            Assert.IsTrue(PAthrzt.AuthErrorMessage.Displayed, "Пользователь не авторизован");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Авторизация клиента с неправильным паролем")]
        public void A3_AuthorizationUserWithWrongPassword()
        {
            PProf.EnterProfileByClickOnIcon("Нажимаю на иконку профиля");
            GF.SendKeys(PAthrzt.EmailInput, "ivantestrwerw@gmail.com", "Ввожу некорректный логин");
            GF.SendKeys(PAthrzt.PasswordInput, "aniartqagmailcom", "Ввожу пароль");
            GF.ClickOn(PAthrzt.SubmitButton, "натискаємо Войти");
            Assert.IsTrue(PAthrzt.AuthErrorMessage.Displayed, "Пользователь не авторизован");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Авторизация клиента через Facebook")]
        public void A4_AuthorizationUserByFacebook()
        {
            PProf.EnterProfileByClickOnIcon("Нажимаю на иконку профиля");
            GF.ClickOn(PAthrzt.FacebookButton, "Выбираю авторизацию через Facebook");
            PAthrzt.FacebookAuth(User.UserFacebook, "Выполняю авторизацию");
            PProf.EnterProfileByClickOnIcon("Нажимаю на иконку профиля");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.PersonalDataButton, "Перехожу в личные данные");
            Assert.IsTrue(PProf.ProfileEmail.GetAttribute("value").Equals(User.UserFacebook.Email), "Клиент авторизирован");
        }
    }
}
