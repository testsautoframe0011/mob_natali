﻿using MobTestFramework.General;
using MobTestFramework.Tests;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TestFramework.Tests.Catalog
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Каталог"), Property("TestSuite:", "CT")]
    class TestCatalog<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Vanya Demenko"), Description("Переход на детальную по цвету")]
        public void CT1_ChangeColourOfItemInCatalogPage()
        {
            GF.NavigateToUrl(PPattern.SaleUrl);
            GF.MoveToElement(PCatalog.ChangeColourButtonCatalog, "рухаюся до товару");
            var title = GF.GetTextOfElement(PCatalog.ProductItemsTitlesList[0]);
            GF.ClickOn(PCatalog.ChangeColourButtonCatalog, "Перехожу на товар другого цвета");

            GF.CheckElementNotAttachedToDom2(PCatalog.ProductItemList);

            Assert.AreNotEqual(PItem.TitleOfItemInPageItem.Text, title, "колір успішно змінений");
        }
    }
}
