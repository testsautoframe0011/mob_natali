﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;
using System.Threading;
using MobTestFramework.General;
using MobTestFramework.Pages;

namespace MobTestFramework.Tests.MobCart.WishList
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Лист Желаний")]
    class TestAddToWishList<TBrowser> : TestBase<TBrowser>
    {
        private readonly string SearchText = "юбка";
        private readonly string IncorrectSearchText = "абвг";
        private readonly int AMOUNT_ITEMS_WISHLIST = 3;
        /* нет возможности добавить товар в лист желаний со страницы товара
                [Test]
                [Category("Vanya Demenko"), Description("Добавление товара в лист желаний через страницу товара")]
                public void AddItemToWishListFromPageItem()
                {
                    PAthrzt.AuthorizationUser(PageAuthorizationUser.UserFirst, "Пользователь авторизируется");
                    PPattern.EnterForUnregistredUserInProfileByClickOnText("Переход в профиль");
                    PProf.DeleteAllItemsFromWishListAuth("Удаление всех товаров с листа желаний");
                    GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
                    GF.ClickOn(PPattern.OpenNashiTrendySubMenuButton, "Раскрываю Базовый офис");
                    GF.ClickOn(PPattern.Vesna21Button, "нажимаю на Весна 21");//GF.ClickOn(PPattern.KapsulnyyGarderobButton, "натискаю на Капсульный гардероб");
                    GF.CheckAction(PV21.ViewFirstObrazVesna21Button.Displayed, "Жду загрузки трендов");


                    GF.ClickOn(PV21.ViewFirstObrazVesna21Button, "Нажимаю на первый образ");
                    GF.ClickOn(GXP.ViewItemInObrazPopup, "Нажимаю на товар с образа");
                    GF.ClickOn(PItem.AddToWishListButtonPegeItem, "Добавляю в Лист желаний");
                    GF.CheckAction(PItem.ItemIsAddedToWishListPegeItem.Displayed, "чекаю додавання елемента до листа");
                    GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
                    GF.ClickOn(PProf.WishListButton, "натискаю на лист бажань");
                    Assert.IsTrue(PProf.FirstItemInWishList.Displayed, "Товар успішно доданий до листа бажань");
                }*/

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в лист желаний через каталог товаров")]
        public void WL1_AddItemToWishListByCatalogPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromWishListAuth("Удаление всех товаров с листа желаний");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.SaleButton, "Нажимаю на Sale");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "Добавляю в Лист желаний первый товар");
            PProf.EnterProfileByClickOnIcon("Захожу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "В списке выбираю Лист желаний");
            Assert.IsTrue(PProf.FirstItemInWishList.Displayed, "Товар добавлен в Лист желаний");
        }
                
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление нескольких товаров в лист желаний")]
        public void WL2_AddSeveralItemsToWishListByCatalogPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromWishListAuth("Удаление всех товаров с листа желаний");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenCatalogSubMenuButton, "Раскрываю Каталог");
            GF.ClickOn(PPattern.PlatyaButton, "Нажимаю на Платья");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к первому товару");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "Добавляю в лист желаний первый товар");
            GF.ClickOn(PCatalog.AddToWishListButtonOfSecondItem, "Добавляю в лист желаний второй товар");
            GF.ClickOn(PCatalog.AddToWishListButtonOfThirdItem, "Добавляю в лист желаний третий товар");
            PProf.EnterProfileByClickOnIcon("Захожу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "В списке выбираю Лист желаний");
            Assert.AreEqual(PProf.ItemsInWishList.Count, AMOUNT_ITEMS_WISHLIST, "Товары добавленны в Лист желаний");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в лист желаний через страницу поиска")]
        public void WL3_AddItemToWishListBySearchPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromWishListAuth("Удаление всех товаров с листа желаний");
            GF.ClickOn(PPattern.SearchButton, "Нажимаю на поиск");
            GF.SendKeys(PPattern.SearchInput, SearchText, "Ввожу текст Юбки");
            GF.ClickOn(PPattern.GoSearchButton, "Нажимаю Найти");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "Добавляю товар в Лист желаний");
            PProf.EnterProfileByClickOnIcon("Захожу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "В списке выбираю Лист желаний");
            Assert.IsTrue(PProf.FirstItemInWishList.Displayed, "Товары добавлен в Лист желаний");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в лист желаний через слайдер Просмотренные товары")]
        public void WL4_AddItemToWishListBySliderViewedItems()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromWishListAuth("Удаление всех товаров с листа желаний");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenSaleSubMenuButton, "Раскрываю Sale");
            GF.ClickOn(PPattern.ObuvSaleButton, "Нажимаю на Обувь");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(PItem.ViewedProductsSliderItemAddToFavorite, "Добавляю в лист желаний товар из слайдера Просмотренные товары");
            PProf.EnterProfileByClickOnIcon("Захожу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "В списке выбираю Лист желаний");
            Assert.IsTrue(PProf.FirstItemInWishList.Displayed, "Товары добавлен в Лист желаний");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в лист желаний через Новые поступления, при неудачном поиске")]
        public void WL5_AddItemToWishListByNewArrivals()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromWishListAuth("Удаление всех товаров с листа желаний");
            GF.ClickOn(PPattern.SearchButton, "Нажимаю на поиск");
            GF.SendKeys(PPattern.SearchInput, IncorrectSearchText, "Ввожу текст абвг");
            GF.ClickOn(PPattern.GoSearchButton, "Нажимаю Найти");
            GF.CheckAction(PPattern.NewArrivalsWishlistButton.Displayed, "Ожидаю загрузки Новых Поступлений");
            GF.ClickOn(PPattern.NewArrivalsWishlistButton, "Добавляю в лист желаний товар из Новых поступлений");
            PProf.EnterProfileByClickOnIcon("Захожу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "В списке выбираю Лист желаний");
            Assert.IsTrue(PProf.FirstItemInWishList.Displayed, "Товары добавлен в Лист желаний");
        }
    }
}
