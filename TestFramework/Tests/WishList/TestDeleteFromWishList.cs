﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using MobTestFramework.General;
using MobTestFramework.Pages;

namespace MobTestFramework.Tests.WishList
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Лист Желаний")]
    class TestDeleteFromWishList<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление товара из листа желаний")]
        public void DeleteItemFromWishList()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromWishListAuth("Удаление всех товаров с листа желаний");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenSaleSubMenuButton, "Раскрываю Sale");
            GF.ClickOn(PPattern.KostyumySaleButton, "Нажимаю на Костюмы");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "Добавляю в Лист желаний первый товар");
            GF.CheckAction(PCatalog.ItemIsAddedToWishList.Displayed, "Жду добавления товара в Лист желаний");
            PProf.EnterProfileByClickOnIcon("Захожу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "В списке выбираю Лист желаний");
            GF.ClickOn(PProf.DeleteFromWishListButton, "видаляю товар з листа бажань");
            Assert.IsTrue(GF.CheckElementNotVisible(PProf.FirstItemInWishList), "Товар удален с Листа желаний");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление товара из листа желаний через каталог")]
        public void DeleteItemFromWishListFromCatalog()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Пользователь авторизируется");
            PProf.DeleteAllItemsFromWishListAuth("Удаление всех товаров с листа желаний");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.NovyePostupleniyaButton, "Нажимаю на Новинки");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "Добавляю в Лист желаний первый товар");
            GF.CheckAction(PCatalog.ItemIsAddedToWishList.Displayed, "Жду добавления товара в Лист желаний");
            PProf.EnterProfileByClickOnIcon("Захожу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "В списке выбираю Лист желаний");
            GF.CheckAction(PProf.DeleteFromWishListButton.Displayed, "Проверяю отображения товара в Листе желаний");
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.NovyePostupleniyaButton, "Нажимаю на Новинки");
            GF.ClickOn(PCatalog.DeleteFromWishListFirstItemButton, "удаляю первый товар из избранного");
            PProf.EnterProfileByClickOnIcon("Захожу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "В списке выбираю Лист желаний");
            Assert.IsTrue(PProf.EmptyWishListText.Displayed, "Товар удален с Листа желаний");
        }
    }
}
