﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using MobTestFramework.General;
using MobTestFramework.Pages;

namespace MobTestFramework.Tests
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Поиск"), Property("TestSuite:", "S")]
    class TestSearch<TBrowser> : TestBase<TBrowser>
    {
        private readonly string SearchText = "платье";
        private readonly string SearchErrorMessage = "К сожалению, не удалось ничего найти по вашему запросу";

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка работоспособности поиска товаров по сайту по категории")]
        public void S1_SearchItems()
        {
            GF.ClickOn(PPattern.SearchButton, "Нажимаю на поиск");
            GF.SendKeys(PPattern.SearchInput, SearchText, "Вводжу текст платье");
            GF.ClickOn(PPattern.GoSearchButton, "Выполняю поиск");
            GF.CheckAction(PCatalog.ProductItemList[0].Displayed, "Ожидаю загрузки товаров");
            Assert.IsTrue(PCatalog.SearchResultContainsCorrectRequest(PCatalog.ProductItemsTitlesList, SearchText), "Заданные товары найдены");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка работоспособности поиска товаров по сайту по названию")]
        public void S2_SearchItemByTitle()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenNashiTrendySubMenuButton, "Раскрываю Наши тренды");
            GF.ClickOn(PPattern.NashiTkaniButton, "Выбираю Наши ткани");
            GF.MoveToElement(PNashiTkany.FirstItemNashiTkani, "Направляюсь к товару с первого слайдера");
            GF.ClickOn(PNashiTkany.FirstItemNashiTkani, "Нажимаю на товар с первого слайдера");
            GF.MoveToElement(PItem.NameOfItemInPageItem);
            var title = GF.GetTextOfElement(PItem.NameOfItemInPageItem,true);
            GF.ClickOn(PPattern.SearchButton, "Нажимаю на поиск");
            GF.SendKeys(PPattern.SearchInput, title, "Ввожу название товара");
            GF.ClickOn(PPattern.GoSearchButton, "Выполняю поиск");
            Assert.IsTrue(GF.GetTextOfElement(PCatalog.ProductItemsTitlesList[0],true).Contains(title), "Необходимый товар найден");
        }


        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка работоспособности поиска товаров по сайту при неверном названии")]
        public void S3_SearchItemsByWrongText()
        {
            GF.ClickOn(PPattern.SearchButton, "Нажимаю на поиск");
            GF.SendKeys(PPattern.SearchInput, "мобилка", "Ввожу некорректный текст");
            GF.ClickOn(PPattern.GoSearchButton, "Выполняю поиск");
            Assert.IsTrue(PCatalog.SearchErrorMessage.Text.Contains(SearchErrorMessage), "Сообщение о неверном запросе отображено");
        }
    }
}
