﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using MobTestFramework.General;

namespace MobTestFramework.Tests
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Образы"), Property("TestSuite:", "O")]
    class TestObrazes<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Переход на страницу товара через попап образа")]
        public void O1_GoToItemPageFromObrazPopup()
        { 
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenNashiTrendySubMenuButton, "Разворачиваю список Наши тренды");
            GF.ClickOn(PPattern.OfficeСollection, "нажимаю на Весна 21");
            GF.CheckAction(PV21.ViewFirstObrazVesna21Button.Displayed, "Жду загрузки трендов");
            Thread.Sleep(3000);
            GF.ClickOn(PV21.ViewFirstObrazVesna21Button, "Открываю попап с образом");
            GF.MoveToElement(GXP.TextOfFirstItemInObrazPopup);
            var title = GF.GetTextOfElement(GXP.TextOfFirstItemInObrazPopup,true);
            GF.ClickOn(GXP.ViewItemInObrazPopup[0], "Нажимаю на товар с образу");
            Assert.AreEqual(GF.GetTextOfElement(PItem.NameOfItemInPageItem,true), title, "Переход на страницу товара выполнен");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Открыть следующий образ в попапе")]
        public void O2_OpenNextObrazInPopupRightArrow()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenNashiTrendySubMenuButton, "Разворачиваю список Наши тренды");
            GF.ClickOn(PPattern.OfficeСollection, "нажимаю на Весна 21");
            GF.CheckAction(PV21.ViewFirstObrazVesna21Button.Displayed, "Жду загрузки трендов");
             GF.ClickOn(PV21.ViewFirstObrazVesna21Button, "Открываю первый образ");          
            var info = GF.GetTextOfElement(GXP.InfoAboutObraz);
            GF.ClickOn(GXP.SliderFroward, "Перехожу к следующему образу");
            Assert.AreNotEqual(GF.GetTextOfElement(GXP.InfoAboutObraz), info, "Переход к следующему образу выполнен");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Открыть предыдущий образ в попапе")]
        public void O3_OpenPreviousObrazInPopup()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenNashiTrendySubMenuButton, "Разворачиваю список Наши тренды");
            GF.ClickOn(PPattern.OfficeСollection, "нажимаю на Весна 21");
            GF.CheckAction(PV21.ViewFirstObrazVesna21Button.Displayed, "Жду загрузки трендов");
            GF.ClickOn(PV21.ViewMiddleObrazVesna21Button, "Открываю образ из середины");
            var info = GF.GetTextOfElement(GXP.InfoAboutObraz);
            GF.ClickOn(GXP.SliderBack, "Переходжу к предыдущему образу");
            Assert.AreNotEqual(GF.GetTextOfElement(GXP.InfoAboutObraz), info, "Попап успішно закритий");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Закрытие попапа c образом")]
        public void O4_CloseObrazPopup()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenNashiTrendySubMenuButton, "Разворачиваю список Наши тренды");
            GF.ClickOn(PPattern.OfficeСollection, "нажимаю на Весна 21");
            GF.CheckAction(PV21.ViewFirstObrazVesna21Button.Displayed, "Жду загрузки трендов");
            GF.ClickOn(PV21.ViewFirstObrazVesna21Button, "Открываю первый образ");
            GF.ClickOn(GXP.CloseObrazPopupButton, "Закрываю образ");
            Thread.Sleep(1000);
            List<IWebElement> popUp = GXP.ObrazPopupList;
            Assert.IsTrue(GF.CheckElementNotExist(popUp), "Попап закрыт");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка корректности расчета суммы в попапе с образом")]
        public void O5_CorrectnessOfCalculationOfAmountObrazPopup()
        {
            GF.ClickOn(PPattern.HamburgerButton, "Нажимаю на кнопку Навигации");
            GF.ClickOn(PPattern.OpenNashiTrendySubMenuButton, "Разворачиваю список Наши тренды");
            GF.ClickOn(PPattern.OfficeСollection, "нажимаю на Весна 21");
            GF.CheckAction(PV21.ViewFirstObrazVesna21Button.Displayed, "Жду загрузки трендов");
            GF.ClickOn(PV21.ViewFirstObrazVesna21Button, "Открываю первый образ");
            GF.MoveToElement(GXP.ListOfPriceItemsObrazPopup[0]);
            GF.CheckAction(GXP.ViewItemInObrazPopup[0].Displayed, "Жду отображение образов");
            int summOfitems = PStayWarm.SummOfPricesOfObrazItems(GXP.ListOfPriceItemsObrazPopup);
            Assert.AreEqual(PStayWarm.GetNumberPrice(GXP.TotalSummOfAmountObrazPopup), summOfitems, "суму підраховано правильно");
        }
    }
}
